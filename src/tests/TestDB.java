import java.sql.*;

/**
 * Created by Dmitriy on 08.03.2015.
 */
public class TestDB {
    private static  Connection connection;
    private static Statement stmt ;
    private PreparedStatement prstmt;
    static String USER = "root";
    static String PASS = "doithard";
    final static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    final static String DB_URL = "jdbc:mysql://localhost:3306/webApp";
    public static ResultSet ExecuteQuery(String query) throws SQLException {
        ResultSet result = null;
        try
        {
            stmt = connection.createStatement();
            result = stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static void init()
    {
        try
        {
            connection = null;
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
        }
        catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] arg) throws SQLException {
        String login = "ZXNnYWx0dXI=";
        String password = "MTIzMzIxMjMzNA==";
        String query = "SELECT* FROM `usersdb` WHERE " +
                "`login` =\""+login+"\" AND `password` =\""+password+"\"";
        init();
       ResultSet resultSet = ExecuteQuery(query);
        if(resultSet.first()) {
            System.out.println(resultSet.getByte("status_activation"));
            System.out.println(resultSet.getString("surname"));
        }
    stmt.close();

    }
}
