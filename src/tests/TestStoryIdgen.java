import cz.esgaltur.webApp.servlets.Add;
import cz.esgaltur.webApp.servlets.classes.City;
import cz.esgaltur.webApp.servlets.classes.Story;
import cz.esgaltur.webApp.servlets.util.DBConn;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Dmitriy on 22.03.2015.
 */
public class TestStoryIdgen {
    public static void main(String args[]) throws SQLException {
        System.out.println( Add.StoryIDGenerator(1));

        System.out.println(City.getAllCitiesMap().get("Bělá pod Pradědem"));
        System.out.println(City.getAllCitiesMap().get(City.odstrDiak("Bělá pod Pradědem")));
        DBConn dbConn  = new DBConn();
        System.out.println(  dbConn.isConnectionSuccessful());
    }
}
