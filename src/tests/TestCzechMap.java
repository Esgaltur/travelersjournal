import cz.esgaltur.webApp.servlets.classes.City;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * TEST
 */
public class TestCzechMap {
    public static void main(String[] args) throws SQLException {
        Map<String,Integer> cities = City.getAllCitiesMap();
        System.out.println(cities.get("Frenstat pod Radhostem"));
  }
}
