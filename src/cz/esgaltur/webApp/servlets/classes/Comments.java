package cz.esgaltur.webApp.servlets.classes;


import cz.esgaltur.webApp.servlets.util.DBConn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Comments {
    public int getStoryID() {
        return storyID;
    }
    public int getUsrID() {
        return usrID;
    }
    public void setUsrID(int usrID) {
        this.usrID = usrID;
    }
    public String getComment() {return comment;}
    public boolean isForAll() {return forAll;}

    public int getCommentID() {
        return commentID;
    }

    public Comments(int commentID,int storyID, int usrID, String comment, boolean forAll) {
        this.storyID = storyID;
        this.usrID = usrID;
        this.comment = comment;
        this.forAll = forAll;
        this.commentID = commentID;

    }
    public Comments(int storyID, int usrID, String comment, boolean forAll) {
        this.storyID = storyID;
        this.usrID = usrID;
        this.comment = comment;
        this.forAll = forAll;

    }
    public Comments(Comments comments) {
        this.storyID = comments.getStoryID();
        this.usrID = comments.getUsrID();
        this.comment = comments.getComment();
        this.forAll = comments.isForAll();
    }
   public static  ArrayList<Comments>  getAllCommentsByStoryID(int StoryID) throws SQLException {
        String query = "SELECT* FROM `comments` "+
                "INNER JOIN `usersdb` ON `comments`.`UsrID` = `usersdb`.`UsrID` WHERE `StoryID`="+StoryID;
        DBConn dbConn = new DBConn();
        ArrayList<Comments> comments = new ArrayList<>();
        dbConn.initialize();
        ResultSet resultSet = null;
        try
        {
            resultSet = dbConn.ExecuteQuery(query);
            while(resultSet.next())
            {
                comments.add(
                        new Comments(resultSet.getInt("CommentID"),resultSet.getInt("StoryID"),
                                       resultSet.getInt("UsrID"),
                                             resultSet.getString("Comment"),
                                                resultSet.getBoolean("ForAll")));
            }

            return comments;
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            dbConn.ResultClose();
            if (resultSet != null) {
                resultSet.close();
            }
        }


        return null;
    }
    public void InsertComment() throws SQLException {

      DBConn dbCon = new DBConn();
        dbCon.initialize();
        dbCon.InsertComment(this);
        dbCon.ResultClose();

    }
    public static boolean DeleteCommentByID(int ID)  {
        DBConn dbConn = new DBConn();
        dbConn.initialize();
            return dbConn.DeleteComment(ID);
    }
    private int commentID = 0;
    private boolean forAll = false;
    private int storyID = 0;
    private int usrID = 0;
    private String comment = null;

}
