package cz.esgaltur.webApp.servlets.classes;


import cz.esgaltur.webApp.servlets.util.DBConn;
import cz.esgaltur.webApp.servlets.util.permissions.StoryPermissions;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class Story implements Serializable {
    private  int StoryID;
    private int UsrID;
    private String titleOfStory;
    private StringBuilder Story;
    private String DateTime;
    private int CountryID;
    private int CityID;
    private String fromDate;
    private String toDate;
    private int rating;
    private StoryPermissions storyPermissions = new StoryPermissions();
    private String address;

    //////////////////////Getterss and setters
    public int getRating() {
        return rating;
    }
    public void setRating(int rating) {
        this.rating = rating;
    }
    public void setRating(String rating) {
        this.rating = StringToStars(rating);
    }
    public String getFromDate() {
        return fromDate;
    }
    public String getToDate() {
        return toDate;
    }
    public String getAddress() {
        return address;
    }
    public  int getCityID() {
        return CityID;
    }
    public int getStoryID() {
        return StoryID;
    }
    public int getUsrID() {
        return UsrID;
    }
    public StringBuilder getStory() {
        return Story;
    }
    public String getDateTime() {
        return DateTime;
    }
    public  int getCountryID() {
        return CountryID;
    }
    public String getTitleOfStory() {
        return titleOfStory;
    }
    public StoryPermissions getStoryPermissions() {
        return storyPermissions;
    }
    public void setStoryID(int storyID) {
        StoryID = storyID;
    }
    public void setUsrID(int usrID) {
        UsrID = usrID;
    }
    public void setTitleOfStory(String titleOfStory) {
        this.titleOfStory = titleOfStory;
    }
    public void setStory(StringBuilder story) {
        Story = story;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public void setCountryID(int countryID) {
        CountryID = countryID;
    }

    public void setCityID(int cityID) {
        CityID = cityID;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public void setStoryPermissions(StoryPermissions storyPermissions) {
        this.storyPermissions = storyPermissions;
    }

    public void setAddress(String address) {
        this.address = address;
    }

//////////////////////End of Section

    /**
     *
     * @param storyID
     * @param usrID
     * @param titleOfStory
     * @param story
     * @param dateTime
     * @param countryID
     * @param cityID
     * @param fromDate
     * @param toDate
     * @param address
     * @param permissions
     * @throws SQLException
     */
    public Story(int  storyID, int usrID,String titleOfStory,
                  StringBuilder story,
                  String dateTime, int countryID,
                  int cityID,int rating, String fromDate, String toDate,
                  String address,StoryPermissions permissions) throws SQLException {
        this.StoryID = storyID;
        this.UsrID = usrID;
        this. Story = story;
        this. DateTime = dateTime;
        this. CountryID = countryID;
        this. CityID = cityID;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.address = address;
        this.titleOfStory = titleOfStory;
        this.SetPermissions(permissions);
        this.rating = rating;
    }

    /**
     *
     * @param story
     * @throws SQLException
     */
    public Story(Story story) throws SQLException {
        this.StoryID = story.getStoryID();
        this.UsrID = story.getUsrID();
        this. Story = story.getStory();
        this. DateTime = story.getDateTime();
        this. CountryID = story.getCountryID();
        this. CityID = story.getCityID();
        this.fromDate = story.getFromDate();
        this.toDate = story.getToDate();
        this.address = story.getAddress();
        this.titleOfStory = story.getTitleOfStory();
        this.SetPermissions(story.getStoryPermissions());
        this.rating = story.getRating();
    }

    /**
     *
     * @param storyID
     * @param usrID
     * @param titleOfStory
     * @param story
     * @param dateTime
     * @param countryID
     * @param cityID
     * @param fromDate
     * @param toDate
     * @param address
     * @throws SQLException
     */
    public Story(int  storyID, int usrID,String titleOfStory,
                 StringBuilder story,
                 String dateTime, int countryID,
                 int cityID,int rating, String fromDate, String toDate,
                 String address) throws SQLException {
        this.StoryID = storyID;
        this.UsrID = usrID;
        this. Story = story;
        this. DateTime = dateTime;
        this. CountryID = countryID;
        this. CityID = cityID;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.address = address;
        this.titleOfStory = titleOfStory;
        this.rating = rating;
    }
    public Story(int  storyID, int usrID,String titleOfStory,
                 StringBuilder story,
                 String dateTime, int countryID,
                 int cityID,String rating, String fromDate, String toDate,
                 String address,StoryPermissions permissions) throws SQLException {
        this.StoryID = storyID;
        this.UsrID = usrID;
        this. Story = story;
        this. DateTime = dateTime;
        this. CountryID = countryID;
        this. CityID = cityID;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.address = address;
        this.titleOfStory = titleOfStory;
        this.setRating(rating);
        this.SetPermissions(permissions.isAll(),permissions.isForMeOnly(),permissions.isForOther());
    }

    /**
     *
     * @throws SQLException
     */
    public void InsertStoryIntoDB() throws SQLException {
      DBConn dbCon = new DBConn();
        dbCon.initialize();
        dbCon.InsertStory(this);
        dbCon.ResultClose();
    }

    /**
     *
     * @param StoryID
     * @return
     * @throws SQLException
     */
    public static boolean isStoryIdInDB(int StoryID) throws SQLException {
        String query = "SELECT StoryID FROM `story` WHERE " +
                "`StoryID` =\""+StoryID+"\"";
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet =null;
        try
        {
            resultSet = dbConn.ExecuteQuery(query);
            if(resultSet.next())
            {
                return true;
            }
        }
       catch(SQLException e)
       {
           e.printStackTrace();
       }
        finally {
            dbConn.ResultClose();
            if (resultSet != null) {
                resultSet.close();
            }
        }

        return false;
    }

    /**
     *
     * @param all
     * @param meOnly
     * @param other
     * @throws SQLException
     */
    public void SetPermissions(boolean all,boolean meOnly, boolean other) throws SQLException {
      this.storyPermissions.setForAll(all);
        this.storyPermissions.setForMeOnly(meOnly);
        this.storyPermissions.setForOther(other);
    }

    /**
     *
     * @param permissions
     * @throws SQLException
     */
    public void SetPermissions(StoryPermissions  permissions) throws SQLException {
        this.storyPermissions.setForAll(permissions.isAll());
        this.storyPermissions.setForMeOnly(permissions.isForMeOnly());
        this.storyPermissions.setForOther(permissions.isForOther());
    }

    /**
     *
     * @param Rating
     * @return
     */
    int StringToStars(String Rating)
{
    switch (Rating)
    {
        case "Excellent":return 5;
        case "VeryGood":return 4;
        case "Good":return 3;
        case "Bad":return 2;


    }
    return 0;
}

    /**
     *
     * @param Rating
     * @return
     */
    public String StarsToString(int Rating)
    {
        switch (Rating)
        {
            case 5:return "Excellent";
            case 4:return "Very good";
            case 3:return "Good" ;
            case 2:return"Bad" ;
            default:return "Good";

        }

    }

    /**
     *
     * @param StoryID
     * @return
     * @throws SQLException
     */
    public static Story getStoryByID(int StoryID) throws SQLException {
        String query = "SELECT * FROM `story` WHERE " +
                "`StoryID` =\""+StoryID+"\"";

        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet =null;
        try
        {
            resultSet = dbConn.ExecuteQuery(query);
            if(resultSet.first())
            {
                StoryPermissions storyPermissions1
                        = StoryPermissions.getPermissionsByStoryID(resultSet.getInt("StoryID"));

               return new Story(
                       resultSet.getInt("StoryID"),
                       resultSet.getInt("UsrID"),
                       resultSet.getString("headOfStory"),
                       new StringBuilder(resultSet.getString("Story")),
                       resultSet.getString("Date"),
                       resultSet.getInt("CountryID"),
                       resultSet.getInt("CityID"),
                       resultSet.getInt("stars"),
                       resultSet.getString("fromDate"),
                       resultSet.getString("toDate"),
                       resultSet.getString("webAddress"),storyPermissions1);

            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            dbConn.ResultClose();
            if (resultSet != null) {
                resultSet.close();
            }
        }
        return null;
    }

    /**
     *
     * @return String to Long
     */
    public long getStamp ()
    {
        return Long.parseLong(this.getDateTime());
    }
    public static ArrayList<Story> getAllStories() throws SQLException {

        String qSelectAllStories = "SELECT * FROM `story` " +
                "INNER JOIN `storiesperm` ON `storiesperm`.`storyID` = `story`.`StoryID`";
        DBConn dbConn = new DBConn();
        ArrayList<Story> stories = new ArrayList<>();
    if(dbConn.isConnectionSuccessful())
    {
        ResultSet resultSet = null;
        try
        {
            resultSet = dbConn.ExecuteQuery(qSelectAllStories);
            System.out.println(resultSet!=null);
            while(resultSet.next())
            {
                stories.add(new Story(resultSet.getInt("StoryID"),
                                resultSet.getInt("UsrID"),
                                resultSet.getString("headOfStory"),
                                new StringBuilder(resultSet.getString("Story")),
                                resultSet.getString("Date"),
                                resultSet.getInt("CountryID"),
                                resultSet.getInt("CityID"),
                                resultSet.getInt("stars"),
                                resultSet.getString("fromDate"),
                                resultSet.getString("toDate"),
                                resultSet.getString("webAddress"),
                                new StoryPermissions(resultSet.getBoolean("ForAll"),
                                        resultSet.getBoolean("ForMeOnly"),resultSet.getBoolean("otherUnReg")))
                );
            }

            return stories;
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            dbConn.ResultClose();
            if (resultSet != null) {
                resultSet.close();
            }
        }
    }
        return null;
    }
    public static boolean DeleteStoryByID(int ID)  {
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        return dbConn.DeleteStory(ID);
    }
    public static boolean EditStory(Story story)
    {
      DBConn conn = new DBConn();
        conn.initialize();
        return conn.EditStory(story);

    }

}
