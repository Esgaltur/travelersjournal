package cz.esgaltur.webApp.servlets.classes;

import cz.esgaltur.webApp.servlets.util.DBConn;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;


import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class User implements Serializable {
    private String surname;
    private String name;
    private String email;
    private String login;

    public String getPassword() {
        return password;
    }

    private String password;
    private String confpassword;
    private int UserID=-1;




    /*----------------------Constructors-------------------------------*/
    public User(String surname, String name, String email, String login,
                String password) {
        this.surname = surname;
        this.name = name;
        this.email = email;
        this.login = login;
        this.password = password;
        this.UserID = getUserID();
    }
    public User(String surname, String name, String email, String login,
                String password, String confpassword) {
        this.surname = surname;
        this.name = name;
        this.email = email;
        this.login = login;
        this.password = password;
        this.UserID = getUserID();
    }
    public User(User user) {
        this.surname = user.getSurname();
        this.name = user.getName();
        this.email = user.getEmail();
        this.login = user.getLogin();
        this.UserID = getUserID();
    }

    public User(String surname, String name, String email, String login
    ) {

        this.surname = surname;
        this.name = name;
        this.email = email;
        this.login = login;
        this.UserID = getUserID();
    }
    /*------------------------------------------*/

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    void setUserID(int userID) {
        this.UserID = userID;
    }

    /**
     *
     * @return 3 if email is in db or 4 if login in db
     */
    public int isEmailOrLoginInDatabase()
    {
      DBConn dbConn = new DBConn();
        dbConn.initialize();
        return dbConn.isEmailOrLoginInDatabase(this.getEmail(),this.getLogin());
    }

    /**
     *
     * @return
     */
    public int getUserID()
    {
        if (UserID!=-1)
            return UserID;
        else
        {
            DBConn dbConn = new DBConn();
            dbConn.initialize();
            int UserID = dbConn.getUserID(this.getEmail(), this.getLogin());
          if(UserID!=-1)
            return UserID;
            else
              return -1;
        }
    }

    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<Story> getAllStoriesByUsrID() throws SQLException {

     DBConn  dbConn = new DBConn();
        dbConn.initialize();
        return dbConn.getAllStoriesByUsrID(this.UserID);
    }

    /**
     *
     * @param UserID
     * @return
     * @throws SQLException
     */
    public static ArrayList<Story> getAllStoriesByUsrID(int UserID) throws SQLException {

        DBConn  dbConn = new DBConn();
        dbConn.initialize();
        return dbConn.getAllStoriesByUsrID(UserID);
    }

    /**
     *
     * @param UsrID
     * @return
     * @throws SQLException
     */
    public static User getUserByID(int UsrID) throws SQLException {
        String qGetUserByID = "SELECT * FROM `usersdb` WHERE " +
                "`UsrID` ="+UsrID;

        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet =null;
        try
        {
            resultSet = dbConn.ExecuteQuery(qGetUserByID);
            if(resultSet.first())
            {
                return new User(
                        resultSet.getString("surname"),
                        resultSet.getString("name"),
                        resultSet.getString("email"),
                        resultSet.getString("login"));
            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            dbConn.ResultClose();
            if (resultSet != null) {
                resultSet.close();
            }
        }

        return null;
    }

    /**
     *
     * @param StoryID
     * @return
     * @throws SQLException
     */
    public static User getUserByStoryID(int StoryID) throws SQLException {
        String qGetUserByStoryID =  "SELECT  `surname` ,  `name` ,  `email` ,  `login` ,  `password`" +
                " FROM usersdb" +
                " INNER JOIN `story` ON `story`.`StoryID` ="+StoryID;
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet =null;
        try
        {
            resultSet = dbConn.ExecuteQuery(qGetUserByStoryID);
            if(resultSet.first())
            {
                return new User(
                        resultSet.getString("surname"),
                        resultSet.getString("name"),
                        resultSet.getString("email"),
                        resultSet.getString("login"),
                        resultSet.getString("password")
                );

            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            dbConn.ResultClose();
            if (resultSet != null) {
                resultSet.close();
            }
        }
        System.out.println("DEBUG BLYA");
        return null;
    }

    /**
     *
     * @param id
     * @return
     */
    public static boolean DeleteUserByID(int id) throws SQLException {
        DBConn dbConn = new DBConn();
        dbConn.initialize();
       return dbConn.DeleteUserByID(id);
    }
    public static boolean DeactivateUserAcc(int id)
    {
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        return dbConn.DeactivateUserById(id);
    }
    public static boolean ActivateUserAcc(int id)
    {
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        return dbConn.ActivateUserById(id);
    }
    public static boolean EditAccountData(int userId,User user) throws SQLException {
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        return   dbConn.EditAccountData(userId,user);

    }
    public static String getPasswordByUserID(int UserID)
    {
        ResultSet result = null;
        String qIsEmailOrLoginInDB = "SELECT* FROM `usersdb` WHERE `UsrID`=" +UserID;

        DBConn dbConn = new DBConn();
        dbConn.initialize();

        try {
            result = dbConn.ExecuteQuery(qIsEmailOrLoginInDB);
            if(result.first())
            {
                return LinksGenerator.Decode(result.getString("password"));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                dbConn.ResultClose();
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}


