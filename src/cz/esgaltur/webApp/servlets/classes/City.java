package cz.esgaltur.webApp.servlets.classes;

import cz.esgaltur.webApp.servlets.util.DBConn;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.sort;

/**
 * Created by Dmitriy on 08.03.2015.
 */
public class City {
    private String CityID;
    //private static ArrayList<String> cities  = new ArrayList<String>();
    private final static StringBuilder citiesSB = new StringBuilder();

    public static String odstrDiak(String str)
    {
        String decomposed = java.text.Normalizer.normalize(str, Form.NFD);
        return  decomposed.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
    }

    public static String findCityByID(int CityID) throws SQLException {

        String query = "SELECT CityName FROM `obce_kompletni` WHERE `CityID`=\""+CityID+"\"";
        String city = null;
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet = dbConn.ExecuteQuery(query);
     if(resultSet.first())
        {
            city = resultSet.getString("CityName");
        }
        dbConn.ResultClose();
        resultSet.close();
        return city;
    } public static int findIDByName(String CityName) throws SQLException {
        String query = "SELECT cityID FROM `obce_kompletni` WHERE `CityName`=\""+CityName+"\"";
        int id = 0;
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet = dbConn.ExecuteQuery(query);
     if(resultSet.first())
        {
            id = resultSet.getInt("CityID");
        }
        dbConn.ResultClose();
        resultSet.close();
        return id;
    }
    public static ArrayList<String> getAllCities() throws SQLException {
        ArrayList<String> cities  = new ArrayList<>();
        String query = "SELECT CityName FROM `obce_kompletni` ";
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet = dbConn.ExecuteQuery(query);
        while(resultSet.next())
        {
            cities.add(odstrDiak(resultSet.getString("CityName").trim()));
        }
        sort(cities);
        resultSet.close();
        dbConn.ResultClose();
        return cities;
    }
    public static Map<String,Integer> getAllCitiesMap() throws SQLException {
        Map<String,Integer> integerStringMap  = new HashMap<>();
        String query = "SELECT *" +
                " FROM `obce_kompletni` ";
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet = dbConn.ExecuteQuery(query);
        while(resultSet.next())
        {

            integerStringMap.put(odstrDiak(resultSet.getString("CityName").trim()),resultSet.getInt("cityID"));
        }
        dbConn.ResultClose();
        resultSet.close();

        return integerStringMap;
    }
    public static StringBuilder getAllCitiesSB() throws SQLException {

        String query = "SELECT CityName FROM `obce_kompletni` ";
        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet = dbConn.ExecuteQuery(query);
        while(resultSet.next())
        {
            citiesSB.append("\"").append(resultSet.getString("CityName")).append("\",");//  out.write();
        }
        dbConn.ResultClose();
        resultSet.close();
        return citiesSB;
    }
    public static String getCityNameByID(int CityID) throws SQLException {

        String query = "SELECT CityName FROM `obce_kompletni` WHERE  `cityID`="+CityID;
        DBConn dbConn = new DBConn();
        String result;
        dbConn.initialize();
        ResultSet resultSet = dbConn.ExecuteQuery(query);
        if(resultSet.first())
        {
         result =  resultSet.getString("CityName");

        }
     else result = null;
        dbConn.ResultClose();
        resultSet.close();
        return result;
    }
}
