package cz.esgaltur.webApp.servlets.util;

/**
 *This class validate Surname ,Name email,login, password with regular expressions
 */
class Validator {



    /**
     *
     * @param srname Surname or Name from registration form
     * @return true if Surname or Name consist of Words From A-Z a-Z _ -
     */
    public static boolean ValidateSurnameName(String srname)
    {
        return srname.matches("^[a-zA-Z0-9_-]${3,20}");

    }
    /**
     *
     * @param login login from registration form
     * @return true if login consist of Words From A-Z a-Z _ -
     * and 3 symbols to 15 symbols long
     */
    public static boolean ValidateLogin(String login)
    {
        return login.matches("^[a-zA-Z0-9_-]{3,15}$");
    }
    public static boolean ValidatePassword(String password)
    {
        return password.matches("^[a-zA-Z0-9_-]{6,20}$");
    }
    public static boolean ValidateEmail(String email)
    {
        return email.matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }
}
