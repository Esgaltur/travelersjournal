package cz.esgaltur.webApp.servlets.util;

/**
 * RegistrationFrom
 */
public class RegistrationFormField {

    public static String getSurname(Boolean gOrb) {
        return gOrb ? "<div class=\"field\">\n" +
                "        <label for=\"surn\">Surname:</label>\n" +
                "        <input type=\"text\" name=\"surname\"required=\"true\" class=\"input\" id=\"surn\"/>\n" +
                "    </div>\n" :
                "    <div class=\"field\">\n" +
                        "        <label for=\"surn\">Surname:</label>\n" +
                        "        <input type=\"text\" name=\"surname\"required=\"true\" " +
                        "        class=\"input\" id=\"surn\" style=\"background-color:red\"/>\n" +
                        "    </div>"
                ;
    }

    public static String getName(Boolean gOrb) {
        return gOrb
                ? "<div class=\"field\">\n" +
                "                    <label for=\"nam\">Name:</label>\n" +
                "                    <input type=\"text\" name=\"name\" value=\"\" required=\"true\"  class=\"input\" id=\"nam\"/>\n" +
                "    </div>\n"
                : "<div class=\"field\">\n" +
                "<label for=\"nam\">Name:</label>\n" +
                "<input type=\"text\" name=\"name\" value=\"\" required=\"true\"  class=\"input\" id=\"nam\""
                + " style=\"background-color:red\"/>\n"
                + "</div>\n";
    }

    public static String getEmail(Boolean gOrb) {
        return gOrb
                ? " <div class=\"field\">\n" +
                "                    <label for=\"mail\">E-mail:</label>\n" +
                "                    <input type=\"email\" name=\"email\" value=\"\" required=\"true\"  class=\"input\" id=\"mail\"/>\n" +
                "    </div>"
                : " <div class=\"field\">\n" +
                "                    <label for=\"mail\">E-mail:</label>\n" +
                "                    <input type=\"email\" name=\"email\" value=\"\" required=\"true\"  class=\"input\" id=\"mail\"" +
                "style=\"background-color:red\"/>\n" +
                "    </div>";
    }

    public static String getLogin(Boolean gOrb) {
        return gOrb
                ? "  <div class=\"field\">\n" +
                "                    <label for=\"log\">Login:</label>\n" +
                "                    <input type=\"text\" name=\"login\" value=\"\" required=\"true\"  class=\"input\" id=\"log\"/>\n" +
                "    </div>"
                : "  <div class=\"field\">\n" +
                "                    <label for=\"log\">Login:</label>\n" +
                "                    <input type=\"text\" name=\"login\" value=\"\" required=\"true\"  class=\"input\" id=\"log\"  " +
                "style=\"background-color:red\"/>\n" +
                "    </div>";
    }

    public static String getPassword(Boolean gOrb)
    {
        return gOrb
                ?
                "  <div class=\"field\">\n" +
                "                        <label for=\"hes\"> Password:</label>\n" +
                "                        <input type=\"password\" name=\"password\" value=\"\" required=\"true\"  class=\"input\" id=\"hes\">\n" +
                "        </div>"
                :
                "  <div class=\"field\">\n" +
                "                        <label for=\"hes\"> Password:</label>\n" +
                "                        <input type=\"password\" name=\"password\"" +
                        " value=\"\" required=\"true\"  class=\"input\" id=\"hes\"" +
                        "style=\"background-color:red\"/>\n" +
                "        </div>";
    }

}
