package cz.esgaltur.webApp.servlets.util;


import cz.esgaltur.webApp.servlets.classes.Comments;
import cz.esgaltur.webApp.servlets.classes.Story;
import cz.esgaltur.webApp.servlets.classes.User;
import cz.esgaltur.webApp.servlets.util.interfaces.AuthDB;
import cz.esgaltur.webApp.servlets.util.permissions.StoryPermissions;

import java.sql.*;
import java.util.ArrayList;

public class DBConn implements AuthDB {
    /**
     *
     */
    private Connection connection;
    private Statement stmt ;
    private PreparedStatement prstmt;
    /*****constructor*/
    /**
     *
     */
    public DBConn()
    {
        try
        {
            connection = null;
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);

        }
        catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    /*****constructor*/
    public boolean initialize()
    {
        if(this.connection!=null)
        {

            stmt = null;
            prstmt=null;
            return true;
        }

return false;
    }

    /**
     *
     * @param email
     * @param login
     * @param password
     * @return
     */
    public boolean  InsertNewPassword(String email, String login, String password)
    {
        String    qNewPass  = "UPDATE  `usersdb` " +
        " SET `password`=?  " +
                "WHERE `email`=? AND `login`=?";
        try {
            prstmt = connection.prepareStatement(qNewPass);
            prstmt.setString(1,password);
            prstmt.setString(2,email);
            prstmt.setString(3,login);
            if(prstmt.executeUpdate()==0)
                return false;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }try {
        prstmt.close();
    } catch (SQLException e) {
        e.printStackTrace();
    }

    return true;
    }


    /**
     *
     * @param user
     * @param pass
     * @param hash
     * @param timestamp
     * @return
     */
    public boolean  InsertIntoUsersDB(User user,String pass,String hash,String timestamp)
    {

        String s = "INSERT INTO `usersdb` (`surname`, `name`, `email`,`login`, " +
        "`password`,`status_activation`," +
        " `hash_act`,`timestamp`)" +
        " VALUES (?,?,?,?,?,?,?,?)";
        try
        {

            if(connection!=null)
            {
                prstmt = connection.prepareStatement(s);
                prstmt.setString(1,user.getSurname());
                prstmt.setString(2, user.getName());
                prstmt.setString(3,user.getEmail());
                prstmt.setString(4,user.getLogin());
                prstmt.setString(5,pass);
                prstmt.setBoolean(6, false);
                prstmt.setString(7, hash);
                prstmt.setString(8, timestamp);
                if(prstmt.executeUpdate()==0)
                    return false;

            }
            else return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        try {
            prstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     *
     * @param UsrID
     * @param activated
     * @return
     */
    public boolean  DeactivateUserById(int UsrID)
    {
        String    qActivation  = "UPDATE  `usersdb` " +
                " SET `status_activation`=?  " +
                "WHERE `UsrID`=?";
        try {
            prstmt = connection.prepareStatement(qActivation);
            prstmt.setBoolean(1, false);
            prstmt.setInt(2, UsrID);
            if(prstmt.executeUpdate()==0)
                return false;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        try {
            prstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
    public boolean  ActivateUserById(int UsrID)
    {
        String    qActivation  = "UPDATE  `usersdb` " +
                " SET `status_activation`=?  " +
                "WHERE `UsrID`=?";
        try {
            prstmt = connection.prepareStatement(qActivation);
            prstmt.setBoolean(1, true);
            prstmt.setInt(2, UsrID);
            if(prstmt.executeUpdate()==0)
                return false;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        try {
            prstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     *
     * @param comments
     * @return
     */
    public boolean InsertComment(Comments comments)
    {
        String qCommentInsert = "INSERT INTO `comments` (`StoryID`, `UsrID`, `Comment`,`ForAll`) " +
                " VALUES (?,?,?,?)";
        try
        {

            prstmt = connection.prepareStatement(qCommentInsert);
            prstmt.setInt(1, comments.getStoryID());
            prstmt.setInt(2, comments.getUsrID());
            prstmt.setString(3,comments.getComment());
            prstmt.setBoolean(4, comments.isForAll());
            if(prstmt.executeUpdate()==0)
                return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        try {
            prstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     *
     * @param id
     * @return
     */
    public boolean DeleteComment(int id)
    {
        String qCommmentDelete = "DELETE FROM `webapp`.`comments` WHERE `comments`.`CommentID` = ?";
        try
        {

            prstmt = connection.prepareStatement(qCommmentDelete);
            prstmt.setInt(1, id);
            prstmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        try {
            prstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     *
     * @param story
     * @return
     */
    public boolean  InsertStory(Story story)
    {

        String qInsertStory = "INSERT INTO `story` (`StoryID`, `UsrID`, `headOfStory`, "+
        "`Story`, `Date`, `CountryID`, `CityID`, `stars`, `fromDate`, `toDate`, `webAddress`)" +
                " VALUES (?,?,?,?,?,?,?,?,?,?,?)";

        try
        {

            prstmt = connection.prepareStatement(qInsertStory);
           prstmt.setInt(1,story.getStoryID());
            prstmt.setInt(2,story.getUsrID());
            prstmt.setString(3,story.getTitleOfStory());
            prstmt.setString(4, story.getStory().toString());
            prstmt.setString(5, story.getDateTime());
            prstmt.setInt(6, story.getCountryID());
            prstmt.setInt(7, story.getCityID());
            prstmt.setInt(8, story.getRating());
            prstmt.setString(9,story.getFromDate());
            prstmt.setString(10,story.getToDate());
            prstmt.setString(11,story.getAddress());
            if(prstmt.executeUpdate()==0)
                return false;
            try {
                prstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            /************************************************************************************
             * Insert Permissions*/
            initialize();
            if(StoryPermissions.getPermissionsByStoryID(story.getStoryID())==null)
            {
                String qInsertStoriesPermission = "INSERT INTO `storiesperm`(`storyID`, `ForAll`, `ForMeOnly`, `otherUnReg`) " +
                        "VALUES (?,?,?,?)";
                prstmt = connection.prepareStatement(qInsertStoriesPermission);
                prstmt.setInt(1,story.getStoryID());
                prstmt.setBoolean(2, story.getStoryPermissions().isAll());
                prstmt.setBoolean(3, story.getStoryPermissions().isForMeOnly());
                prstmt.setBoolean(4, story.getStoryPermissions().isForOther());
                if(prstmt.executeUpdate()==0)
                    return false;
            }
        else
                return false;

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        try {
            prstmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return true;
    }

    /**
     *
     * @param email
     * @param hash
     * @param timestamp
     * @param act
     * @return
     */
    public boolean  UpdateStatusActivation(String email,String hash,String timestamp,Boolean act)
    {
        String qChengeStatusActivation= "UPDATE  `usersdb`" +
                "  SET `status_activation`= ?  " +
                "WHERE `email`= ? AND `usersdb`.`hash_act`= ? AND `timestamp`= ?";
        try
        {
            prstmt = connection.prepareStatement(qChengeStatusActivation);
            prstmt.setBoolean(1,act);
            prstmt.setString(2,email);
            prstmt.setString(3,hash);
            prstmt.setString(4, timestamp);
             prstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     *
     * @param query
     * @return
     * @throws SQLException
     */
    public ResultSet ExecuteQuery(String query) throws SQLException {
    ResultSet result = null;
        try
        {
            stmt = connection.createStatement();
            result = stmt.executeQuery(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }
    public void ResultClose()
    {

            try {
                if(stmt != null)
                stmt.close();
                if( prstmt!=null)
                    prstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

    }
    public boolean DeleteStory(int id) {

            String qDeleteStory = "DELETE FROM `webapp`.`story` WHERE `story`.`StoryID` = ?";
            try
            {
                prstmt = connection.prepareStatement(qDeleteStory);
                prstmt.setInt(1, id);
                prstmt.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
            ResultClose();

        /***********************************************************/
        initialize();
        String qDeleteStoryPerm = "DELETE FROM `webapp`.`storiesperm` WHERE `storiesperm`.`storyID`=?";
        try
        {
            prstmt = connection.prepareStatement(qDeleteStoryPerm);
            prstmt.setInt(1, id);
            prstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        finally{
                ResultClose();
        }
            return true;
        }

    /**
     *
     * @param story
     * @return
     */
    public boolean  EditStory(Story story)
    {
        String qChangeStory    =
                "UPDATE  `story` SET  " +
                "`headOfStory`= ?,"+
                "`Story`= ?, " +
                "`Date`= ?, " +
                "`CityID`= ?," +
                " `stars`= ?, " +
                "`fromDate`= ?," +
                " `toDate`= ?" +
                " WHERE `StoryID`= ?";
        try
        {
            prstmt = connection.prepareStatement(qChangeStory);
            prstmt.setString(1,story.getTitleOfStory().trim());
            prstmt.setString(2, story.getStory().toString().trim());
            prstmt.setString(3, story.getDateTime().trim());
            prstmt.setInt(4, story.getCityID());
            prstmt.setInt(5, story.getRating());
            prstmt.setString(6, story.getFromDate().trim());
            prstmt.setString(7,story.getToDate().trim());
            prstmt.setInt(8, story.getStoryID());
            prstmt.executeUpdate();
                ResultClose();

            /***********************************/
            initialize();
            if(StoryPermissions.getPermissionsByStoryID(story.getStoryID())==null)
            {
                String qInsertStoriesPermission = "INSERT INTO `storiesperm`(`storyID`, `ForAll`, `ForMeOnly`, `otherUnReg`) " +
                        "VALUES (?,?,?,?)";
                prstmt = connection.prepareStatement(qInsertStoriesPermission);
                prstmt.setInt(1,story.getStoryID());
                prstmt.setBoolean(2, story.getStoryPermissions().isAll());
                prstmt.setBoolean(3, story.getStoryPermissions().isForMeOnly());
                prstmt.setBoolean(4, story.getStoryPermissions().isForOther());
               prstmt.executeUpdate();
                    ResultClose();
            }
            else
            {
                String qChangeStoriesPermission = "UPDATE `storiesperm` SET `ForAll`=?, `ForMeOnly`=?, `otherUnReg`=? WHERE  `storyID`=?" ;
                prstmt = connection.prepareStatement(qChangeStoriesPermission);
                prstmt.setBoolean(1, story.getStoryPermissions().isAll());
                prstmt.setBoolean(2, story.getStoryPermissions().isForMeOnly());
                prstmt.setBoolean(3, story.getStoryPermissions().isForOther());
                prstmt.setInt(4, story.getStoryID());
                prstmt.executeUpdate();
            }
            prstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        finally{

                ResultClose();
        }
        return true;
    }

    /**
     *
     * @param id
     * @return
     */
    public boolean DeleteUserByID(int id) throws SQLException {
        ArrayList<Story> stories = Story.getAllStories();
        String qDeleteUserByID = "DELETE FROM `webapp`.`usersdb` WHERE `usersdb`.`UsrID` = ?";

        try
        {

            prstmt = connection.prepareStatement(qDeleteUserByID);
            prstmt.setInt(1, id);
            prstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        finally{
                ResultClose();
        }
        /*********************************************/
        initialize();
        String qDeleteAllStoriesOfUser ="DELETE FROM  `story` WHERE  `UsrID` = ?";
        try
        {
            prstmt = connection.prepareStatement(qDeleteAllStoriesOfUser);
            prstmt.setInt(1, id);
            prstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        finally{
                ResultClose();
        }
        String qDeleteAllStoriesPerm ="DELETE FROM `webapp`.`storiesperm` WHERE `storiesperm`.`id` = ?";
      for(Story story: stories)
      {
          initialize();
          if(story.getUsrID()==id)
          {
              try
              {
                  prstmt = connection.prepareStatement(qDeleteAllStoriesPerm);
                  prstmt.setInt(1, story.getStoryID());
                  prstmt.executeUpdate();
              } catch (SQLException e) {
                  e.printStackTrace();
                  return false;
              }
              finally{
                      ResultClose();
              }
          }
      }

        return true;
    }
    public int getUserID(String Email,String Login)
    {
        ResultSet result = null;
        String qGetUserID = "SELECT* FROM `usersdb` WHERE `email`=\"" +
               Email+ "\" AND `login`=\"" +
                Login + "\"";

        DBConn dbConn = new DBConn();
        dbConn.initialize();

        try {
            result = dbConn.ExecuteQuery(qGetUserID);
            if(result.first())
            {
                return result.getInt("UsrID");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                dbConn.ResultClose();
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }
    /**
     *
     * @return 3 if email is in db or 4 if login in db
     */
    public int isEmailOrLoginInDatabase(String Email, String Login)
    {
        ResultSet result = null;
        String qIsEmailOrLoginInDB = "SELECT* FROM `usersdb` WHERE `email`=\"" +
                Email+ "\" OR `login`=\"" +
                Login+ "\"";

        DBConn dbConn = new DBConn();
        dbConn.initialize();

        try {
            result = dbConn.ExecuteQuery(qIsEmailOrLoginInDB);
            if(result.next())
            {
                return  result.getString("email")!=null?
                        3:result.getString("login")!=null?4:0;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                dbConn.ResultClose();
                if (result != null) {
                    result.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return -1;
    }
    /**
     *
     * @return
     * @throws SQLException
     */
    public ArrayList<Story> getAllStoriesByUsrID(int UserID) throws SQLException {

        String qGetAllStoriesbyID =  "SELECT * FROM `story` \n" +
                "INNER JOIN `storiesperm` ON " +
                "`storiesperm`.`storyID` = `story`.`StoryID` WHERE `story`.`usrID`="+UserID;
        ArrayList<Story> stories = new ArrayList<>();
        ResultSet resultSet = null;
        initialize();
        try
        {
            resultSet = ExecuteQuery(qGetAllStoriesbyID);
            while(resultSet.next())
            {
                stories.add(new Story(resultSet.getInt("StoryID"),
                        resultSet.getInt("UsrID"),
                        resultSet.getString("headOfStory"),
                        new StringBuilder(resultSet.getString("Story")),
                        resultSet.getString("Date"),
                        resultSet.getInt("CountryID"),
                        resultSet.getInt("CityID"),
                        resultSet.getInt("stars"),
                        resultSet.getString("fromDate"),
                        resultSet.getString("toDate"),
                        resultSet.getString("webAddress"),
                        new StoryPermissions(resultSet.getBoolean("ForAll"),
                                resultSet.getBoolean("ForMeOnly"),resultSet.getBoolean("otherUnReg"))
                ));

            }
            return stories;
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            ResultClose();
            if (resultSet != null) {
                resultSet.close();
            }
        }


        return null;
    }

    public boolean EditAccountData(int userid,User newUser) throws SQLException {


        User user = User.getUserByID(userid);
        if(!user.getSurname().equals(LinksGenerator.Encode(newUser.getSurname())))
        {
            String qEditAccountDatas =
                    "UPDATE `usersdb` SET `surname`=? WHERE `UsrID`="+userid;
            prstmt = connection.prepareStatement(qEditAccountDatas );
            prstmt.setString(1, newUser.getSurname());
            prstmt.executeUpdate();
                ResultClose();
        }
        if(!user.getName().equals(LinksGenerator.Encode(newUser.getName())))
        {
            initialize();
            String qEditAccountDatas =
                    "UPDATE `usersdb` SET `name`=? WHERE `UsrID`="+userid;
            prstmt = connection.prepareStatement(qEditAccountDatas );
            prstmt.setString(1, newUser.getName());
            prstmt.executeUpdate();
                ResultClose();
        }
        if(LinksGenerator.Decode(user.getEmail()).equals(newUser.getEmail()))
        {
            System.out.println(user.getEmail()+" "+newUser.getEmail());
            initialize();
            User.DeactivateUserAcc(userid);
            SendEmails sendEmails = new SendEmails();
            sendEmails.SendActivationMail(userid);
            String qEditAccountDatas =
                    "UPDATE `usersdb` SET `email`=? WHERE `UsrID`="+userid;
            prstmt = connection.prepareStatement(qEditAccountDatas );
            prstmt.setString(1, newUser.getEmail());
            prstmt.executeUpdate();
                ResultClose();
        }
        if(newUser.getPassword()!=null&&!newUser.getPassword().isEmpty())
        {
            boolean b = newUser.getPassword().equals(User.getPasswordByUserID(userid));
            if(!b)
            {
                initialize();
                String qEditAccountDatas =
                        "UPDATE `usersdb` SET `password`=? WHERE `UsrID`="+userid;
                prstmt = connection.prepareStatement(qEditAccountDatas );
                prstmt.setString(1, newUser.getPassword());
                prstmt.executeUpdate();
                ResultClose();
            }

        }
                ResultClose();
        return true;
    }
    public boolean isConnectionSuccessful()
    {
        if(this.initialize())
        {
          return true;
        }
        else
        {
            System.out.println("DEBUG IS CONSUCC");
            try {
                throw new Exception("Database not works!");
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

    }
}