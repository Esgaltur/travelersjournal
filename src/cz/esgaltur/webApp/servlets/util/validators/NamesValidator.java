package cz.esgaltur.webApp.servlets.util.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Дмитрий on 21.02.2015.
 */
public class NamesValidator {
    private Pattern pattern;
    private Matcher matcher;

    private static final String USERNAME_PATTERN = "^[a-zA-Z0-9_-]{3,50}$";

    public NamesValidator(){
        pattern = Pattern.compile(USERNAME_PATTERN);
    }

    /**
     * Validate password with regular expression
     * @param username Surname or Name for validation
     * @return true valid Surname or Name, false invalid Surname or Name
     */
    public boolean validate(final String username){

        matcher = pattern.matcher(username);
        return matcher.matches();

    }
}
