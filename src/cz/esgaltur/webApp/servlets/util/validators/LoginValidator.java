package cz.esgaltur.webApp.servlets.util.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LoginValidator {
    private Pattern pattern;
    private Matcher matcher;

    private static final String LOGIN_PATTERN = "^[a-z0-9_-]{3,15}$";

    public LoginValidator(){
        pattern = Pattern.compile(LOGIN_PATTERN);
    }

    /**
     * Validate password with regular expression
     * @param login password for validation
     * @return true valid login, false invalid login
     */
    public boolean validate(final String login){

        matcher = pattern.matcher(login);
        return matcher.matches();

    }
}
