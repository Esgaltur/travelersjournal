package cz.esgaltur.webApp.servlets.util.interfaces;


import cz.esgaltur.webApp.servlets.classes.Comments;
import cz.esgaltur.webApp.servlets.classes.Story;
import cz.esgaltur.webApp.servlets.classes.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface AuthDB {
    String USER = "****";
     String PASS = "****";
    final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    final String DB_URL = "jdbc:mysql://localhost:3306/webApp";
    public boolean  InsertNewPassword(String email, String login, String password);
    public boolean  InsertIntoUsersDB(User user,String pass,String hash,String timestamp);
    public boolean  UpdateStatusActivation(String email,String hash,String timestamp,Boolean act);
    public ResultSet ExecuteQuery(String query) throws SQLException ;
    public void ResultClose() throws SQLException;
    public boolean initialize();
    public boolean  InsertStory(Story story);
    public boolean  EditStory(Story story);
    public boolean DeleteStory(int id);
    public boolean InsertComment(Comments comments);
    public boolean DeleteComment(int id);

}
