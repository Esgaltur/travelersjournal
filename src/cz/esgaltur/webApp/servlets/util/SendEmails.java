package cz.esgaltur.webApp.servlets.util;


import cz.esgaltur.webApp.servlets.classes.User;
import cz.esgaltur.webApp.servlets.util.interfaces.AuthData;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.SQLException;
import java.util.Properties;

public class SendEmails implements AuthData {
    @Override
    public  boolean SendPasswordEmail(String email,String login,String password) {
        boolean b = false;
        String mes;
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {

                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(SendMailUsername, SendMailPassword);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SendMailUsername));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject("New Password On Travelers Journal.com");
            mes ="<p>Hi, it's very bad that you have forget password" +
                    "<p>Here is your login and password:<br>" +
                    "<p>login:"+login+"<br><p>password:"+password
                    +"<br>Travelers Journal.com";
            message.setHeader("'Content-type:", " text/html;charset=UTF-8");
            message.setContent(mes, "text/html");
            try{
                Transport.send(message);
            }
            catch (javax.mail.SendFailedException e)
            {

                b = false;
                e.printStackTrace();
                return b;
            }
        }
        catch (MessagingException e)
        {

            b = false;
            e.printStackTrace();
            return b;

        }
        b = true;
        return b;
    }
    public  boolean SendChangesEmail(String email,String login,String password) {
        boolean b = false;
        String mes;
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {

                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(SendMailUsername, SendMailPassword);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SendMailUsername));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setSubject("Password on Travelers Journal.com");
            mes ="<p>Hi, it's very bad that you have forget password" +
                    "<p>Here is your login and password:<br>" +
                    "<p>login:"+login+"<br><p>password:"+password
                    +"<br>Travelers Journal.com";
            message.setHeader("'Content-type:", " text/html;charset=UTF-8");
            message.setContent(mes, "text/html");
            try{
                Transport.send(message);
            }
            catch (javax.mail.SendFailedException e)
            {

                b = false;
                e.printStackTrace();
                return b;
            }
        }
        catch (MessagingException e)
        {

            b = false;
            e.printStackTrace();
            return b;

        }
        b = true;
        return b;
    }
    public void SendActivationMail(int userid) throws SQLException {
        User user = User.getUserByID(userid);
        String mes;
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator()
                {

                    @Override
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication(SendMailUsername, SendMailPassword);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SendMailUsername));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(LinksGenerator.Decode(user.getEmail())));
            message.setSubject("Activation Mail");
            mes ="Welcome to Travelers Journal!<br>" +
                    " please click this link for activate " +
                    "your account! Thins link is active for 24 hours<br>" +
                    "http://www.travelersjournal.cz:8080/web/account/activate.do?usrid="+userid+"email="+user.getEmail()+"&activate=1";
            message.setHeader("'Content-type:", " text/html;charset=UTF-8");
            message.setContent(mes, "text/html");
            try{
                Transport.send(message);

            }
            catch (javax.mail.SendFailedException e)
            {
                System.out.println(e.getMessage()+user.getEmail());
            }

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void SendActivationMail(String email,String hash ,String time )
    {
        String mes;
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator()
                {

                    @Override
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication(SendMailUsername, SendMailPassword);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SendMailUsername));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(LinksGenerator.Decode(email)));
            message.setSubject("Activation Mail");
            mes ="Welcome to Travelers Journal!<br>" +
                    " please click this link for activate " +
                    "your account! Thins link is active for 24 hours<br>" +
                    "http://www.travelersjournal.cz:8080/web/account/activate.do?email="+email+//http://www.travelersjournal.cz:8080/web/account/activate.do
                    "&hash="+hash+"&stamp=" +time+"&activate=1";
            message.setHeader("'Content-type:", " text/html;charset=UTF-8");
            message.setContent(mes, "text/html");
            try{
                Transport.send(message);

            }
            catch (javax.mail.SendFailedException e)
            {
                System.out.println(e.getMessage()+email);
            }

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
    @Override
    public void SuccesfulActivationMail(String email,String hash ,String time )
    {
        String mes;
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator()
                {

                    @Override
                    protected PasswordAuthentication getPasswordAuthentication()
                    {
                        return new PasswordAuthentication(SendMailUsername, SendMailPassword);
                    }
                });
        /* getPasswordAuthentication()->
        {
          return new PasswordAuthentication(SendMailUsername,SendMailPassword);
        });*/

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SendMailUsername));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(LinksGenerator.Decode(email)));
            message.setSubject("The Acctivation has been successful");
            mes ="Welcome to Travelers Journal!<br>" +
                    "<p>This is a place where you can:" +
                    "<ul>" +
                    "<li>Share your stories of travel</li>" +
                    "<li>Saw shared stories of other people</li>"
                    +"<li>Check out places where has you been</li>" +
                    "<li>Choose plases to go to</li>"
                    +"</ul>";
            message.setHeader("'Content-type:", " text/html;charset=UTF-8");
            message.setContent(mes, "text/html");
            try{
                Transport.send(message);

            }
            catch (javax.mail.SendFailedException e)
            {
                System.out.println(e.getMessage()+email);
            }

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
