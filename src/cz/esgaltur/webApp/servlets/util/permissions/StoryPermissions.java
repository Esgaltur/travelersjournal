package cz.esgaltur.webApp.servlets.util.permissions;

import cz.esgaltur.webApp.servlets.util.DBConn;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Dmitriy on 22.03.2015.
 */
public class StoryPermissions {
    public StoryPermissions(boolean all, boolean meOnly, boolean other) {
        this.all = all;
        this.meOnly = meOnly;
        this.other = other;
    }
    public StoryPermissions() {
        this.all = true;
        this.meOnly = false;
        this.other = true;
    }
    public static StoryPermissions getPermissionsByStoryID(int StoryID) throws SQLException {
       // String query = "SELECT * FROM `storiesperm` WHERE " +
           //     "`StoryID`= "+ StoryID;
        String query = "SELECT  `storiesperm`.`ForAll` , " +
                " `storiesperm`.`ForMeOnly` , `storiesperm`.`otherUnReg` \n" +
                "FROM  `storiesperm` \n" +
                "WHERE  `storiesperm`.`StoryID`="+StoryID;

        DBConn dbConn = new DBConn();
        dbConn.initialize();
        ResultSet resultSet =null;
        try
        {
            resultSet = dbConn.ExecuteQuery(query);
            if(resultSet.next())
            {
                return new StoryPermissions(resultSet.getBoolean("ForAll"),
                        resultSet.getBoolean("ForMeOnly"),resultSet.getBoolean("otherUnReg"));

            }
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        finally {
            dbConn.ResultClose();
            if (resultSet != null) {
                resultSet.close();
            }
        }

        return null;
    }
    public boolean isAll() {
        return all;
    }

    public void setForAll(boolean all) {
        this.all = all;
    }

    public boolean isForMeOnly() {
        return meOnly;
    }

    public void setForMeOnly(boolean meOnly) {
        this.meOnly = meOnly;
    }

    public boolean isForOther() {
        return other;
    }

    public void setForOther(boolean other) {
        this.other = other;
    }
    private boolean all;
    private boolean meOnly;
    private boolean other;


}
