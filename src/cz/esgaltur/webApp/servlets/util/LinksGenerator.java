package cz.esgaltur.webApp.servlets.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Class to generate links
 */
public class LinksGenerator {
    /**
     * To generate link for activation you need:
     * @param email email from db
     * @param hash hash which compute from [@link Registration] class
     * @param time time stamp of registration
     * @return Link to activate account
     */
    public static String getMes(String email,String hash ,String time) {
        return
                "http://www.travelersjournal.cz:8080/web/account/activate.do?email="+email+"&hash="+hash+"&stamp="+time+"&activate=1";

    }

    /**
     *
     * @param str String to encode with Base64
     * @return Encoded string
     */
    public static String Encode(String str)
    {
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(
                str.getBytes(StandardCharsets.UTF_8) );
    }
    /**
     * This is special function for encoding timestamp cause of timestamp is long
     * @param time timestamp of registration
     * @return Encoded timestamp with Base64
     */
    public static String Encode(long time)
    {
        String str = Long.toString(time);
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(
                str.getBytes(StandardCharsets.UTF_8) );


    }
    /**
     *
     * @param str String to decode with Base64
     * @return Decoded string
     */
    public static String Decode(String str)
    {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] decodedByteArray = decoder.decode(str);
        return new String(decodedByteArray);
    }


}
