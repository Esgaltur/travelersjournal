package cz.esgaltur.webApp.servlets.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sosnovich Dmitriy
 * @version 1.0.0.0
 */
public class WebPages {
    /**
     * Map of webpages
     */
    private  Map<String,String> webPages;
    /**
     *
     */
    private static WebPages SINGLETON = new WebPages();
    private WebPages(){
        initialize();
    }
    public Map<String, String> getWebPages() {
        return webPages;
    }
    public static   WebPages getInstance()
    {
        return SINGLETON;
    }
    public String getWebPage(String key) {
        return webPages.get(key);
    }
    private void initialize()
    {
        webPages = new HashMap<String, String>();

        webPages.put("BadSurname","<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\">\n" +
                "    <title></title>\n" +
                "    <link rel=\"stylesheet\" href=\"css/RegStyle.css\">\n" +
                "    <link rel=\"stylesheet\" href=\"css/footer.css\">\n" +
                "    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>\n" +
                "    <script src=\"http://code.jquery.com/jquery-1.10.2.js\"></script>\n" +
                "    <script type=\"text/javascript\">//<![CDATA[\n" +
                "    $(window).load(function(){\n" +
                "        $(\"[data-toggle]\").click(function() {\n" +
                "            var toggle_el = $(this).data(\"toggle\");\n" +
                "            $(toggle_el).toggleClass(\"open-sidebar\");\n" +
                "        });\n" +
                "\n" +
                "\n" +
                "    });//]]>\n" +
                "\n" +
                "    </script>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "<div class=\"container\">\n" +
                "\n" +
                "    <div class=\"main-content\">\n" +
                "<header>\n" +
                "    <h3 class=\"headText\">Travelers Journal</h3>\n" +
                "    </header>\n" +
                "        <div class=\"content\">\n" +
                "            <div class=\"content-body\">\n" +
                "\n" +
                "<form action=\"/registration.do\" method=\"post\" class=\"registration\">\n" +
                "    <h3 class=\"Text\">Registration</h3>\n" +
                "<div class=\"field\">\n" +
                "                <label for=\"surn\">Surname:</label>\n" +
                "                <input type=\"text\" name=\"surname\"required=\"true\" class=\"input\" id=\"surn\" style=\"background-color:redd\">\n" +
                "</div>\n" +
                "<div class=\"field\">\n" +
                "                <label for=\"nam\">Name:</label>\n" +
                "                <input type=\"text\" name=\"name\" value=\"\" required=\"true\"  class=\"input\" id=\"nam\">\n" +
                "</div>\n" +
                "<div class=\"field\">\n" +
                "                <label for=\"mail\">E-mail:</label>\n" +
                "                <input type=\"text\" name=\"email\" value=\"\" required=\"true\"  class=\"input\" id=\"mail\">\n" +
                "</div>\n" +
                "<div class=\"field\">\n" +
                "                <label for=\"log\">Login:</label>\n" +
                "                <input type=\"text\" name=\"login\" value=\"\" required=\"true\"  class=\"input\" id=\"log\">\n" +
                "</div>\n" +
                "<div class=\"field\">\n" +
                "                <label for=\"hes\"> Password:</label>\n" +
                "                <input type=\"password\" name=\"password\" value=\"\" required=\"true\"  class=\"input\" id=\"hes\">\n" +
                "</div>\n" +
                "    <div class=\"field\">\n" +
                "        <label for=\"confpas\">Confirm Password::</label>\n" +
                "        <input type=\"password\" value=\"\" name =\"confPass\"   id=\"confpas\" class=\"input\">\n" +
                "    </div>\n" +
                "\n" +
                "    <input type=\"submit\" class=\"button\" value=\"Register\" >\n" +
                "\n" +
                "\n" +
                "</form>\n" +
                "<div class=\"backgroundPict\"></div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "\n" +
                "</div>\n" +
                "<%@include file=\"footer.inc.jsp\"%>\n" +
                "</body>\n" +
                "</html>");
        webPages.put("PassMailSent","" +

                "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "    <meta http-equiv=\"content-type\" content=\"text/html\" charset=UTF-8\\>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>Your account need to be activated</title>\n" +
                "    <link rel=\"stylesheet\" href=\"css/RegStyle.css\">\n" +
                "    <link rel=\"stylesheet\" href=\"css/footer.css\">\n" +
                "    <style>\n" +
                "        #inputs input\n" +
                "        {\n" +
                "            background: url(https://cdn2.iconfinder.com/data/icons/ourea-icons/128/Email_256x256-32.png) no-repeat;\n" +
                "            background-size: 40px;\n" +
                "\n" +
                "            padding: 15px 15px 15px 30px;\n" +
                "            margin: 0 0 10px 0;\n" +
                "            width: 88.25%;/*353px;  353 + 2 + 45 = 400 */\n" +
                "            border: 1px solid #ccc;\n" +
                "            -moz-border-radius: 5px;\n" +
                "            -webkit-border-radius: 5px;\n" +
                "            border-radius: 5px;\n" +
                "            -moz-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;\n" +
                "            -webkit-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;\n" +
                "            box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;\n" +
                "        }\n" +
                "        #inputs input:focus\n" +
                "        {\n" +
                "            background-color: #fff;\n" +
                "            border-color: #e8c291;\n" +
                "            outline: none;\n" +
                "            -moz-box-shadow: 0 0 0 1px #e8c291 inset;\n" +
                "            -webkit-box-shadow: 0 0 0 1px #e8c291 inset;\n" +
                "            box-shadow: 0 0 0 1px #e8c291 inset;\n" +
                "        }\n" +
                "        #submit\n" +
                "        {\n" +
                "            background-color: #ffb94b;\n" +
                "            background-image: -webkit-gradient(linear, left top, left bottom, from(#fddb6f), to(#ffb94b));\n" +
                "            background-image: -webkit-linear-gradient(top, #fddb6f, #ffb94b);\n" +
                "            background-image: -moz-linear-gradient(top, #fddb6f, #ffb94b);\n" +
                "            background-image: -ms-linear-gradient(top, #fddb6f, #ffb94b);\n" +
                "            background-image: -o-linear-gradient(top, #fddb6f, #ffb94b);\n" +
                "            background-image: linear-gradient(top, #fddb6f, #ffb94b);\n" +
                "            -moz-border-radius: 3px;\n" +
                "            -webkit-border-radius: 3px;\n" +
                "            border-radius: 3px;\n" +
                "            text-shadow: 0 1px 0 rgba(255,255,255,0.5);\n" +
                "            -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;\n" +
                "            -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;\n" +
                "            box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;\n" +
                "            border-width: 1px;\n" +
                "            border-style: solid;\n" +
                "            border-color: #d69e31 #e3a037 #d5982d #e3a037;\n" +
                "            float: left;\n" +
                "            height: 35px;\n" +
                "            padding: 0;\n" +
                "            width: 200px;\n" +
                "            cursor: pointer;\n" +
                "            font: bold 15px Arial, Helvetica;\n" +
                "            color: #8f5a0a;\n" +
                "            margin-top:10px;\n" +
                "        }\n" +
                "\n" +
                "        #submit:hover,#submit:focus\n" +
                "        {\n" +
                "            background-color: #fddb6f;\n" +
                "            background-image: -webkit-gradient(linear, left top, left bottom, from(#ffb94b), to(#fddb6f));\n" +
                "            background-image: -webkit-linear-gradient(top, #ffb94b, #fddb6f);\n" +
                "            background-image: -moz-linear-gradient(top, #ffb94b, #fddb6f);\n" +
                "            background-image: -ms-linear-gradient(top, #ffb94b, #fddb6f);\n" +
                "            background-image: -o-linear-gradient(top, #ffb94b, #fddb6f);\n" +
                "            background-image: linear-gradient(top, #ffb94b, #fddb6f);\n" +
                "        }\n" +
                "\n" +
                "        #submit:active\n" +
                "        {\n" +
                "            outline: none;\n" +
                "            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;\n" +
                "            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;\n" +
                "            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;\n" +
                "        }\n" +
                "\n" +
                "        #submit::-moz-focus-inner\n" +
                "        {\n" +
                "            border: none;\n" +
                "        }\n" +
                "\n" +
                "        #actions\n" +
                "        {\n" +
                "            margin: 25px 0 0 0;\n" +
                "        }\n" +
                "        fieldset\n" +
                "        {\n" +
                "            border: 0;\n" +
                "            padding: 0;\n" +
                "            margin: 0;\n" +
                "        }\n" +
                "\n" +
                "#genpass{\n" +
                "    background-color: #fff;\n" +
                "    background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));\n" +
                "    background-image: -webkit-linear-gradient(top, #fff, #eee);\n" +
                "    background-image: -moz-linear-gradient(top, #fff, #eee);\n" +
                "    background-image: -ms-linear-gradient(top, #fff, #eee);\n" +
                "    background-image: -o-linear-gradient(top, #fff, #eee);\n" +
                "    background-image: linear-gradient(top, #fff, #eee);\n" +
                "    height: 100px;\n" +
                "    width: 300px;\n" +
                "    margin: -300px 0 0 -230px;\n" +
                "    padding: 30px;\n" +
                "    position: absolute;\n" +
                "    top: 50%;\n" +
                "    left: 50%;\n" +
                "    z-index: 0;\n" +
                "    -moz-border-radius: 3px;\n" +
                "    -webkit-border-radius: 3px;\n" +
                "    border-radius: 3px;\n" +
                "    -webkit-box-shadow:\n" +
                "    0 0 2px rgba(0, 0, 0, 0.2),\n" +
                "    0 1px 1px rgba(0, 0, 0, .2),\n" +
                "    0 3px 0 #fff,\n" +
                "    0 4px 0 rgba(0, 0, 0, .2),\n" +
                "    0 6px 0 #fff,\n" +
                "    0 7px 0 rgba(0, 0, 0, .2);\n" +
                "    -moz-box-shadow:\n" +
                "    0 0 2px rgba(0, 0, 0, 0.2),\n" +
                "    1px 1px   0 rgba(0,   0,   0,   .1),\n" +
                "    3px 3px   0 rgba(255, 255, 255, 1),\n" +
                "    4px 4px   0 rgba(0,   0,   0,   .1),\n" +
                "    6px 6px   0 rgba(255, 255, 255, 1),\n" +
                "    7px 7px   0 rgba(0,   0,   0,   .1);\n" +
                "    box-shadow:\n" +
                "    0 0 2px rgba(0, 0, 0, 0.2),\n" +
                "    0 1px 1px rgba(0, 0, 0, .2),\n" +
                "    0 3px 0 #fff,\n" +
                "    0 4px 0 rgba(0, 0, 0, .2),\n" +
                "    0 6px 0 #fff,\n" +
                "    0 7px 0 rgba(0, 0, 0, .2);\n" +
                "}\n" +
                "    </style>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div class=\"container\">\n" +
                "    <div class=\"main-content\">\n" +
                "        <div class=\"content\">\n" +
                "            <div class=\"header\">\n" +
                "\n" +
                "            </div>\n" +
                "            <div class=\"content-body\">\n" +
                "                     <span style=\"margin-left:50px;\">Email with new password has been sent</span>                     "
                +
                "            </div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</div>\n" +
                "<%@include file=\"footer.inc.jsp\"%>\n" +
                "</body>\n" +
                "</html>");

    }

}
