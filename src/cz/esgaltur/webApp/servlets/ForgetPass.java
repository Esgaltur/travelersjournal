package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.util.DBConn;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;
import cz.esgaltur.webApp.servlets.util.SendEmails;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;
import java.time.Instant;


public class ForgetPass extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SendEmails sendEmails = new SendEmails();
        ResultSet result;
        String emailFromForm = LinksGenerator.Encode(request.getParameter("email").trim());
        String query = "SELECT * FROM `usersdb` WHERE `email` =\"" + emailFromForm + "\"";
        DBConn conn = new DBConn();
        long time = Instant.now().getEpochSecond();
        String password;
        String login;
        String email;
        HttpSession session = request.getSession();
        conn.initialize();
        try {
            result = conn.ExecuteQuery(query);
            if (result.first()) {
                email = result.getString(4);
                login = result.getString(5);
                password = LinksGenerator.Decode(result.getString(6));
                boolean t = conn.InsertNewPassword(email, login, password);
                if (t) {
                    if (!sendEmails.SendPasswordEmail(LinksGenerator.Decode(email), LinksGenerator.Decode(login), password)) {
                        response.setContentType("text/html;charset=utf-8 ");
                        response.sendRedirect("./fgtpass.jsp?act=" + LinksGenerator.Encode("NoNetworkConnection" + time));
                    } else {
                        response.setContentType("text/html;charset=utf-8 ");
                        response.sendRedirect("./fgtpass.jsp?act=" + LinksGenerator.Encode("SuccessfulSendEmail" + time));

                    }
                } else {
                    response.sendError(527, "Not to possible insert new password");
                }
            } else {
                response.setContentType("text/html;charset=utf-8 ");
                response.sendRedirect("./fgtpass.jsp?act=" + LinksGenerator.Encode("badEmail" + time));

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendError(403, "Forbidden");
    }


}
