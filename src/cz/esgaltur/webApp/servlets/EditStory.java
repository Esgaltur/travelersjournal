package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.classes.City;
import cz.esgaltur.webApp.servlets.classes.Story;
import cz.esgaltur.webApp.servlets.util.permissions.StoryPermissions;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;

/**
 *
 */
@WebServlet(name = "EditStory")
/**
 * Servlet to Edit stories by story id
 */

public class EditStory extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StringBuilder storyStr  = new StringBuilder(request.getParameter("story"));
        int cityID = 0;
        String from = request.getParameter("from").trim();
        String to = request.getParameter("to").trim();
        String rating = request.getParameter("rating").trim();
        String nameOfStory = request.getParameter("headOfStory").trim();
        String dateTime = Long.toString(Instant.now().getEpochSecond());
        Boolean all = request.getParameter("radio").equals("All");
        Boolean me = request.getParameter("radio").equals("MeOnly");
        Boolean  other = request.getParameter("other")!=null;
        int UsrID = 0;
        int StoryID = Integer.parseInt(request.getParameter("storyID").trim());
        StoryPermissions storyPermissions = new StoryPermissions(all,me,other);
        HttpSession session = request.getSession();
        if (session.getAttribute("UsrID")!=null) {
            UsrID = Integer.parseInt((session.getAttribute("UsrID").toString()));
        }
      //  else response.sendError(666,"There is no User Name");
        try
        {
            cityID = City.getAllCitiesMap().get(City.odstrDiak(request.getParameter("city")));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String address = "story.jsp?id="+StoryID;
        session.setAttribute("StoryID",StoryID);
        try {
            Story story = new Story(StoryID,
                    UsrID,
                    nameOfStory,
                    storyStr,
                    dateTime, 1,
                    cityID,rating,
                    from, to, address,storyPermissions);
            if(Story.EditStory(story))//if insert is successful
            {
               ///web/stories/editstory.do
               response.sendRedirect("../feed/");
            }
          //  else  response.sendError(666,"ERROR");//Error



        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(403,"Don't go here");
    }
}
