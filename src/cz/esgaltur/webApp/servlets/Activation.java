package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.classes.User;
import cz.esgaltur.webApp.servlets.util.DBConn;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;
import cz.esgaltur.webApp.servlets.util.SendEmails;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Date;
import java.util.Locale;


public class Activation extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(403,"Forbidden");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

       if(request.getParameter("hash")!=null&&request.getParameter("email")!=null&&request.getParameter("stamp")!=null)
       {
           try{
               /**
                * Write to Webpage
                */
               PrintWriter out = response.getWriter();
               /**
                * New instance of DBConn to connect to database
                */
               DBConn conn     = new DBConn();
               /**
                * Get Parameter Email. To send an activation mail
                */
               String email    = request.getParameter("email").trim();

               /**
                * Timestamp to send via response
                */
               long time       = new Date().toInstant().getEpochSecond();
               /**
                * Get parameter hash to send an activation email
                */
               String hash     = request.getParameter("hash").trim();
               /**
                * Get Parameter timestamp of the registration
                */
               long timestamp  = Long.valueOf( LinksGenerator.Decode(request.getParameter("stamp").trim()));
               /**
                * query to Update status activation with email hash and timestamp
                */
//initialization of DBConn
               conn.initialize();
               if (conn.UpdateStatusActivation(email, hash,LinksGenerator.Encode(timestamp) , true)) {
                   response.setContentType("text/html;charset=utf-8 ");
                   response.sendRedirect("./activation.jsp?act=" + LinksGenerator.Encode("ActivationFromLinkSuccessful" + time));
               } else {
                   response.sendError(527,"Error");
               }
                   conn.ResultClose();

           }catch(IllegalArgumentException e)
           {
            response.sendError(500,"ERRor");
               e.printStackTrace();
           }
       }
       else if(request.getParameter("email")!=null&&request.getParameter("usrid")!=null)
        {
            String email    = request.getParameter("email").trim();
            Integer usrid    = Integer.valueOf(request.getParameter("usrid").trim());
            long time       = new Date().toInstant().getEpochSecond();
            User.ActivateUserAcc(usrid);
            response.sendRedirect("./activation.jsp?act=" + LinksGenerator.Encode("ActivationFromLinkSuccessful" + time));

        }
        }



}
