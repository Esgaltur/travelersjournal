package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.util.DBConn;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

/**
 * Authorising Servlet
 * Sent cookies to panel.jsp
 */

public class Auth extends HttpServlet {

    private int UsrID        = 0;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String query        = "";
        String login        = "";
        String password     = "";
        String surName      = "";
        String name         = "";
        String hash         = "";
        byte activated      = 0;
        String email        = "";
        ResultSet result    = null;
        String time ;
        DBConn conn= null;

        long timeBadLogin   = 0;
        if((request.getParameter("login").trim()!=null) && (request.getParameter("password").trim()!=null))
        {
            login        = LinksGenerator.Encode(request.getParameter("login").trim());
            password     = LinksGenerator.Encode(request.getParameter("password").trim());
            conn = new DBConn();

            conn.initialize();
            query = "SELECT* FROM `usersdb` WHERE " +
                    "`login` =\""+login+"\" AND `password` =\""+password+"\"";
            try {
                result = conn!=null?conn.ExecuteQuery(query):null;
                if(!result.first())
                {

                    response.setHeader("Content-Type","text/html; charset=utf-8");
                    timeBadLogin = Instant.now().getEpochSecond();
                    HttpSession session = request.getSession();
                    session.setAttribute("timeBadLogin", String.valueOf(timeBadLogin));
                    response.sendRedirect("./?act=" + LinksGenerator.Encode("badlogin" + timeBadLogin));
                    conn.ResultClose();
                }
                else
                {
                    result.first();

                    activated = result.getByte("status_activation");
                    if (activated == 0)
                    {
                        email = result.getString("email");
                        hash = result.getString("hash_act");
                        time = result.getString("timestamp");
                        String s = LinksGenerator.getMes(email,hash,time);
                        HttpSession session = request.getSession();
                        response.setHeader("Content-Type", "text/html; charset=utf-8");
                        session.setAttribute("regLink",s);
                        response.sendRedirect("../account/activation.jsp");
                        conn.ResultClose();
                    }
                    else
                    {
                        UsrID =  result.getInt("UsrID");
                        surName = LinksGenerator.Decode(result.getString(2));
                        name = LinksGenerator.Decode(result.getString(3));
                        HttpSession session = request.getSession();
                        String user = surName + " " + name;
                        session.setAttribute("UsrID",Integer.toString(UsrID));
                        session.setAttribute("login",login);
                        session.setAttribute("userName",user);
                        session.setAttribute("SessID", session.getId());
                        session.setMaxInactiveInterval(300 * 600);
                        response.setHeader("Content-Type", "text/html; charset=utf-8");
                        response.sendRedirect("../panel.jsp");
                        conn.ResultClose();
                    }
                }


            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        else
        {
          response.sendError(-1,"ERROR");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendError(403,"Forbidden");
    }



}
/*
*/