package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.classes.User;
import cz.esgaltur.webApp.servlets.util.DBConn;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;
import cz.esgaltur.webApp.servlets.util.validators.LoginValidator;
import cz.esgaltur.webApp.servlets.util.validators.NamesValidator;
import cz.esgaltur.webApp.servlets.util.validators.PasswordValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;


@WebServlet(name = "EditAccountDatas")
public class EditAccountDatas extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try {
            String surname = LinksGenerator.Encode(request.getParameter("firstName").trim());
            String name = LinksGenerator.Encode(request.getParameter("lastName").trim());
            String email = LinksGenerator.Encode(request.getParameter("email").trim());
            String login = LinksGenerator.Encode(request.getParameter("login").trim());
            String oldPass = LinksGenerator.Encode(request.getParameter("oldPass").trim());
            String password = LinksGenerator.Encode(request.getParameter("newPass").trim());
            String confPass = LinksGenerator.Encode(request.getParameter("confNewPass").trim());
            long time = Instant.now().getEpochSecond();
            int userId = Integer.valueOf(request.getParameter("userid").trim());
            DBConn dbConn = new DBConn();
            if (dbConn.isConnectionSuccessful()) {

                    User user = new User(surname, name, email, login, password, confPass);
                    try {
                  if(dbConn.EditAccountData(userId, user))
                      response.sendRedirect(request.getHeader("Referer"));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }


                    dbConn.ResultClose();
            }
           else response.sendError(505,"Connection with DB was not successful");
        }
            catch(NullPointerException e)
            {
                e.printStackTrace();
            }
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(403,"Forbidden");
    }
    int overParams(String surname, String name, String login, String password, String confpass) {
        LoginValidator loginValidator = new LoginValidator();
        NamesValidator namesValidator = new NamesValidator();
        PasswordValidator passwordValidator = new PasswordValidator();
        if (!namesValidator.validate(LinksGenerator.Decode(surname))) {
            return 1;

        }
        if (!namesValidator.validate(LinksGenerator.Decode(name))) {
            return 2;
        }
        if (!loginValidator.validate(LinksGenerator.Decode(login))) {
            return 4;
        }
        if (!passwordValidator.validate(LinksGenerator.Decode(password))||!password.equals(confpass)) {
            return 5;
        }
        return 0;
    }

}
