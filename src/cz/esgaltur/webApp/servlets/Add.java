package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.classes.City;
import cz.esgaltur.webApp.servlets.classes.Story;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;
import cz.esgaltur.webApp.servlets.util.permissions.StoryPermissions;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Random;

@WebServlet(name = "Add")
public class Add extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        StringBuilder storyStr  = new StringBuilder(request.getParameter("story"));
        int cityID = 0;
        String from = request.getParameter("from").trim();
        String to = request.getParameter("to").trim();
        String rating = request.getParameter("rating").trim();
        String nameOfStory = request.getParameter("headOfStory").trim();
        String dateTime = Long.toString(Instant.now().getEpochSecond());
        Boolean all = request.getParameter("radio").equals("All");
        Boolean me = request.getParameter("radio").equals("MeOnly");
        Boolean  other = request.getParameter("other")!=null;
        StoryPermissions storyPermissions = new StoryPermissions(all,me,other);
        int StoryID = 0;
        int UsrID = 0;
        try
        {

        cityID = City.getAllCitiesMap().get(request.getParameter("city"));


        StoryID = StoryIDGenerator(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String address = "story.jsp?id="+StoryID;
      HttpSession session = request.getSession();
        /**if*/
        if (session.getAttribute("UsrID")!=null) { UsrID = Integer.parseInt((session.getAttribute("UsrID").toString()));}
        else
            response.sendError(666,"There is no User Name");
        /*if*/
        session.setAttribute("StoryID",StoryID);
        session.setAttribute("address",address);
        session.setAttribute("other",other.toString());

        try {
            Story story = new Story(StoryID,
                                    UsrID,
                                    nameOfStory,
                                    storyStr,
                                    dateTime, 1,
                                     cityID,rating,
                                     from, to, address,storyPermissions);
            story.InsertStoryIntoDB();
            String addressRedir = !other ?"./add.jsp?act="+ LinksGenerator.Encode("AddStorySuccessful&Other=0"):"./add.jsp?act="+ LinksGenerator.Encode("AddStorySuccessful");
            response.sendRedirect(addressRedir);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
response.sendError(403,"Don't go here");
    }
    public static int StoryIDGenerator(int StoryID) throws SQLException
    {
        Random random = new Random();
        int result = random.nextInt(10000);
        while(true)
        {
            if(Story.isStoryIdInDB(result))
            {
                result =  random.nextInt(10000);
            }
            else
            {
                return result;
            }

        }
    }
}
