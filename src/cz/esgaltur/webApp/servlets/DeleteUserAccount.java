package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.classes.User;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Dmitriy on 31.03.2015.
 */
@WebServlet(name = "DeleteUserAccount")
public class DeleteUserAccount extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int userid = 0;
        String action = null;
        if(request.getParameter("act")!=null&&request.getParameter("uid")!=null) {
            userid = Integer.valueOf(LinksGenerator.Decode(request.getParameter("uid")));
            action = LinksGenerator.Decode(request.getParameter("act"));
            switch (action) {
                case "del": {
                    try {
                        if (User.DeleteUserByID(userid)) {
                            response.sendRedirect("../auth/logout.do");
                        } else
                            response.sendError(-1, "User does not exists");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case "deact": {

                        if (User.DeactivateUserAcc(userid)) {

                            response.sendRedirect("../auth/logout.do");
                        } else
                            response.sendError(-2, "Deactivation not successful");

                    break;
                }
            }
        }

    }
}
