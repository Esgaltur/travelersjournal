package cz.esgaltur.webApp.servlets;


import cz.esgaltur.webApp.servlets.classes.User;
import cz.esgaltur.webApp.servlets.util.DBConn;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;
import cz.esgaltur.webApp.servlets.util.SendEmails;
import cz.esgaltur.webApp.servlets.util.validators.LoginValidator;
import cz.esgaltur.webApp.servlets.util.validators.NamesValidator;
import cz.esgaltur.webApp.servlets.util.validators.PasswordValidator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;

/**
 * Servlet which receive data to register;
 * Then connect to db and Insert new record
 * This is Registration
 */
@WebServlet(name = "Registration")
public class Registration extends HttpServlet  {
    private static String mes;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String surname        = LinksGenerator.Encode(request.getParameter("surname").trim());
    String name           = LinksGenerator.Encode(request.getParameter("name").trim());
    String email          = LinksGenerator.Encode(request.getParameter("email").trim());
    String login          = LinksGenerator.Encode(request.getParameter("login").trim());
    String password       = LinksGenerator.Encode(request.getParameter("password").trim());
    String confPass       = LinksGenerator.Encode(request.getParameter("confPass").trim());
    int    res            = 0;
    DBConn conn           = new DBConn();
    SendEmails sendEmails = new SendEmails();
    conn.initialize();
        User user = new User(surname ,name ,email, login,password );
        String time = LinksGenerator.Encode(Instant.now().getEpochSecond());
        if(user.isEmailOrLoginInDatabase()==3)
            res=3;
       else if(user.isEmailOrLoginInDatabase()==4)
            res=4;
        else
            res = overParams(surname ,name ,login ,password , confPass);
        String hash = Integer.toString(31*Math.abs((surname + name + email + login + password + confPass).hashCode()));
        switch (res) {
            case 0: {

                conn.InsertIntoUsersDB(user,password, hash, time);
                response.setContentType("text/html;charset=utf-8 ");
                sendEmails.SendActivationMail(email, hash, time);
                response.sendRedirect("registration.jsp?act="+LinksGenerator.Encode("RegSuccessful" + time));

                break;
            }
            case 1: {
                response.setContentType("text/html;charset=utf-8 ");
                response.sendRedirect("registration.jsp?act="+LinksGenerator.Encode("BadSurname" + time));
                break;
            }
            case 2: {
                response.setContentType("text/html;charset=utf-8 ");
                response.sendRedirect("registration.jsp?act=" + LinksGenerator.Encode("BadName" + time));
                break;
            }
            case 3: {
                response.setContentType("text/html;charset=utf-8 ");
                response.sendRedirect("registration.jsp?act=" + LinksGenerator.Encode("ExistingEmail" + time));
                break;
            }
            case 4: {
                response.setContentType("text/html;charset=utf-8 ");
                response.sendRedirect("registration.jsp?act=" + LinksGenerator.Encode("BadOrExistingLogin" + time));
                break;
            }
            case 5: {
                response.setContentType("text/html;charset=utf-8 ");
                response.sendRedirect("registration.jsp?act=" + LinksGenerator.Encode("BadPassword" + time));
                break;
            }

        }
     conn.ResultClose();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(403,"Forbidden");
    }

    int overParams(String surname, String name, String login, String password, String confpass) {
        LoginValidator loginValidator = new LoginValidator();
        NamesValidator namesValidator = new NamesValidator();
        PasswordValidator passwordValidator = new PasswordValidator();
        if (!namesValidator.validate(LinksGenerator.Decode(surname))) {
            return 1;

        }
        if (!namesValidator.validate(LinksGenerator.Decode(name))) {
            return 2;
        }
        if (!loginValidator.validate(LinksGenerator.Decode(login))) {
            return 4;
        }
        if (!passwordValidator.validate(LinksGenerator.Decode(password))||!password.equals(confpass)) {
            return 5;
        }
        return 0;
    }


}
