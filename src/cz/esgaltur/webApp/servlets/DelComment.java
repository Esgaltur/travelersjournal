package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.classes.Comments;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Dmitriy on 30.03.2015.
 */
@WebServlet(name = "DelComment")
public class DelComment extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(403,"Forbidden");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String id = request.getParameter("id");
        if(id!=null)
        {
            Comments.DeleteCommentByID(Integer.parseInt(id));
            response.sendRedirect(request.getHeader("Referer"));
        }
        else
            response.sendError(403,"Forbidden");
    }
}
