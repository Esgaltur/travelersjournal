package cz.esgaltur.webApp.servlets;

import cz.esgaltur.webApp.servlets.classes.Comments;
import cz.esgaltur.webApp.servlets.util.LinksGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Dmitriy on 29.03.2015.
 */
@WebServlet(name = "AddComment")
public class AddComment extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            int StoryID         = 0;
            int UsrID           = 0;
            boolean forAll      = false;
            String comment      = null;
            String activeTab    = null;
            Comments comments   = null;
        if( request.getParameter("StoryID")!=null)
        {
            StoryID = Integer.parseInt(request.getParameter("StoryID"));
            UsrID = Integer.parseInt(request.getParameter("UsrID"));
            forAll = request.getParameter("ForAll")!=null;
            comment = request.getParameter("comment");
            activeTab = request.getParameter("activeTab");
            comments = new Comments(StoryID,UsrID,comment,forAll);
        try
        {
             comments.InsertComment();
        } catch (SQLException e)
        {
              e.printStackTrace();
        }
        response.sendRedirect(request.getHeader("Referer"));
        }
        else
            response.sendError(404,"Error, number of story");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(403,"Forbidden");
    }
}
