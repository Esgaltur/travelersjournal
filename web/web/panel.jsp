
<!DOCTYPE html>
<html>
<head>
    <%

        String login = null;
        if (session.getAttribute("login") == null||  session.getAttribute("UsrID")==null) {
            response.sendRedirect("./auth/");
        } else login = (String) session.getAttribute("login");
        String userName =(String) session.getAttribute("userName");
        String userID =(String) session.getAttribute("UsrID");
        String sessionID = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("userName")) userName = cookie.getValue();
                if (cookie.getName().equals("SessID")) sessionID = cookie.getValue();
            }
        } else {
            sessionID = session.getId();
        }
    %>
    <style>

    </style>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Main panel</title>
    <!---Styles--->
      <link rel="stylesheet" href="../css/panel.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!---Styles--->
    <!---JavaScript--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="../js/menu.js"></script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $.slidebars();

            });
        }) (jQuery);
    </script>
    <!---JavaScript--->
</head>

<body>
<header>
    <span>
    <h3 class="header">Travelers Journal</h3>
    <h3 class="usrName">Hello, <%=userName%></h3>
        </span>
</header>
<div class="sb-slidebar sb-left">
    <a href="./panel.jsp">Main Panel</a>
    <a href="./stories/add.jsp">Add your travel story</a>
    <a href="./stories/my/">Your stories</a>
    <a href="./feed/">New shared stories</a>
    <a href="./account">Account settings</a>
    <a href="./auth/logout.do">Sign out</a>
</div>
<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">
    </div>

</div>


</body>
<%@include file="../jsp.inc/footer.inc.jsp" %>
</html>