<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.Story" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.User" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.time.Instant" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.City" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.Comments" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%
        ArrayList<Story> stories=null;
        String userName =null;
        String userI = null;
        String Active = null;
        String userID =null;

        try {
            if (session.getAttribute("login") == null ||session.getAttribute("UsrID")==null)
            {
                response.sendRedirect("../auth/logout.do");
            }
            else
            {
                String login = null;
                //  =  Integer.parseInt(session.getAttribute("UsrID").toString());


               userName =(String) session.getAttribute("userName");
                userID =(String) session.getAttribute("UsrID");
               login =  session.getAttribute("login").toString();
                stories = Story.getAllStories();
                Active  = request.getParameter("atab")!=null?request.getParameter("atab"):null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    %>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>All shared stories</title>
    <!---Styles--->
    <link rel="stylesheet" href="../.././css/mystories.css">
    <link rel="stylesheet" href="../.././css/menu.css">
    <link rel="stylesheet" href="../.././css/footer.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!---Styles--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="../.././js/menu.js"></script>
    <script type="text/javascript">
      <%
    if(Active!=null)
    {
    out.print(" (function($) {\n" +
"          $(document).ready(function() {\n" +
"              $.slidebars();\n" +
"\n" +
"              $( \"#accordion\" ).accordion({\n" +
"                  heightStyle: \"content\",\n" +
"                  active:"+Active+""+
"              });\n" +
"          });\n" +
"\n" +
"      }) (jQuery);");
    }
    else
    out.print(" (function($) {\n" +
"          $(document).ready(function() {\n" +
"              $.slidebars();\n" +
"\n" +
"              $( \"#accordion\" ).accordion({\n" +
"                  heightStyle: \"content\"\n" +
"              });\n" +
"          });\n" +
"\n" +
"      }) (jQuery);");
      %>
    </script>
</head>
<body>
<header>
    <span>
    <h3 class="header">Travelers Journal</h3>
    <h3 class="usrName">Hello, <%=userName%></h3>
        </span>
</header>
<div class="sb-slidebar sb-left">
        <a href="../panel.jsp">Main Panel</a>
        <a href="../stories/add.jsp">Add your travel story</a>
        <a href="../stories/my/">Your stories</a>
        <a href="../feed/">New shared stories</a>
        <a href="../account">Account settings</a>
        <a href="../auth/logout.do">Sign out</a>

</div>
<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">
        <%
            int i = 0;
            if(stories!=null)
            {
                out.print("<div id=\"accordion\">");
                for(Story story: stories)
                {
                    User user = null;
                    ArrayList<Comments> comments=null;
                    try {
                        comments = Comments.getAllCommentsByStoryID(story.getStoryID());
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    if(story.getStoryPermissions().isAll())
                    {
                        try {
                            user = User.getUserByID(story.getUsrID());
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        i++;
                        try {
                            out.print("<h3 id=\"story"+i+"\">Story #"+i+":</h3>");
                            out.print("<div>\n");
                            if(story.getStoryPermissions().isForOther())
                            {
                                out.print("<h3> Address</h3>\n");
                                out.print("<div>\n");
                                out.print(story.getAddress());
                                out.print("</div>\n");
                            }
                            out.print("<h3> Title:</h3>\n");
                            out.print("<div>\n");
                            out.print(" "+story.getTitleOfStory());
                            out.print(" </div>\n");
                            out.print("\n<h3>City:</h3>");
                            out.print("\n <div>");
                            out.print("\n "+ City.findCityByID(story.getCityID()));
                            out.print("\n </div>");
                            out.print("\n<h3> Dates:</h3>");
                            out.print("\n<div>");
                            out.print("\n From:"+story.getFromDate());
                            out.print("\n To:"+story.getToDate());
                            out.print("\n</div>");
                            out.print("\n<h3> Rating:</h3>");
                            out.print("\n<div>");
                            out.print("\n "+story.StarsToString(story.getRating() ) );
                            out.print("\n</div>");
                            out.print("\n<h3>Text:</h3>");
                            out.print("\n<div>");
                            out.print("\n "+story.getStory().toString());
                            out.print("\n</div> ");
                            out.print("\n<h3 style=\"margin-left:85%;\">Creation Data</h3>");
                            out.print("\n<div style=\"margin-left:75%;\">");
                            out.print("\n Created: "+ Instant.ofEpochSecond(story.getStamp()));
                            out.print("\n <br>By:"+ LinksGenerator.Decode(user.getLogin()));
                            out.print("\n<div id=\"manipulate\">");
                            if(Integer.valueOf(userID)==story.getUsrID())
                                out.print("<br><a id=\"edit\" href=\"../stories/edit.jsp?sid="+story.getStoryID()+"\">Edit</a>");

                            if(Integer.valueOf(userID)==story.getUsrID())
                                out.print("<br><a id=\"delete\" href=\"../stories/delstory.do?id="+story.getStoryID()+"\">Delete</a>");
                            out.print("\n</div>");
                            out.print("\n</div>");
                            out.print("\n<h3>Comments</h3>");
                            out.print("\n<div>");

                            if(comments==null||comments.size()==0)
                            {
                                out.print("\nNo comments<br> Leave first  Comment");
                                out.print("<form action=\"../stories/addcomment.do\" method=\"post\">");
                                out.print("<textarea required name=\"comment\" " +
                                        "style=\" margin-top: 50px;\n" +
                                        "    width:100%;\n" +
                                        "    height:200px;border:1px soolid blue;\"></textarea>");
                                out.print("<input type=\"text\" hidden=\"hidden\" value=\""+story.getStoryID()+"\"name=\"StoryID\"/>");
                                out.print("<input type=\"text\" hidden=\"hidden\" value=\""+i+"\"name=\"activeTab\"/>");
                                out.print("<input type=\"text\" hidden=\"hidden\" value=\""+userID+"\"name=\"UsrID\"/>");
                                out.print("<input type=\"checkbox\"  value=\"ForAll\" name=\"ForAll\" style=\"color:black;\"/>Show this comment to unregistered users");
                                out.print("<br><input type=\"submit\" value=\"Send comment to this story\"/>");
                                out.print("</form>");
                            }
                            else
                            {
                                for(Comments comment:comments)
                                {
                                    User usrCom = User.getUserByID(comment.getUsrID());
                                    out.print("<h3>"+LinksGenerator.Decode(usrCom.getLogin())+"</h3>");
                                    out.print("\n<div>");
                                    out.print(comment.getComment());
                                    out.print("\n<br>");
                                    if(Integer.valueOf(userID)==comment.getUsrID())
                                        out.print("<a id=\"delete\" href=\"../stories/delcomment.do?id="+comment.getCommentID()+"\">Delete</a>");

                                    out.print("</div>");
                                }
                                out.print("<form action=\"../stories/addcomment.do\" method=\"post\">");
                                out.print("<textarea name=\"comment\" " +
                                        "style=\" margin-top: 50px;\n" +
                                        "    width:100%;\n" +
                                        "    height:200px;border:1px solid blue;\"></textarea>");
                                out.print("<input type=\"text\" hidden=\"hidden\" value=\""+story.getStoryID()+"\"name=\"StoryID\"/>");
                                out.print("<input type=\"text\" hidden=\"hidden\" value=\""+userID+"\"name=\"UsrID\"/>");
                                out.print("<input type=\"text\" hidden=\"hidden\" value=\""+i+"\"name=\"activeTab\"/>");
                                out.print("<input type=\"checkbox\"  value=\"ForAll\" name=\"ForAll\" style=\"color:black;\"/>Show this comment to unregistered users");
                                out.print("<br><input type=\"submit\" value=\"Send comment to this story\"/>");
                                out.print("</form>");
                            }
                            out.print("</div>");
                            out.print("</div>");
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            else
                out.print("<h1 style=\"color:red;\">Nothing to show. There is no stories</h1>");


        %>
    </div>
</div></div>
</div>

<%@include file="../.././jsp.inc/footer.inc.jsp" %>
</body>

</html>