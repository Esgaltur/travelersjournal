<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login page</title>
    <link rel="stylesheet" href="../../css/authStyle.css">
    <link rel="stylesheet" href="../../css/loginForm.css">
    <link rel="stylesheet" href="../../css/footer.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

    <%
        String badLogin = " ";
        if (request.getParameter("act") != null) {

            if (LinksGenerator.Decode(request.getParameter("act")).contains("badLogin")) ;
            {
                badLogin = "Your login or password is not correct!";
            }


        }
    %>
</head>
<body>
<header>
    <h3 class="header">Travelers Journal</h3>
</header>
<div class="container">
    <div class="main-content">
        <div class="content">
            <div class="content-body">
                <form action="/web/auth/auth.do" method="post" id="login">
                    <h1>Log In</h1>
                    <fieldset id="inputs">
                        <input id="username" type="text" placeholder="Username" name="login" autofocus required>
                        <input id="password" type="password" placeholder="Password" name="password" required>
                    </fieldset>
                    <fieldset id="actions">
                        <input type="submit" id="submit" value="Log in">
                        <a href="./fgtpass.jsp">Forgot your password?</a><a href="../../registration.jsp">Register</a>
                    </fieldset>
                    <fieldset>
                        <span style="color:red; margin-left:50px;"><%=badLogin%></span>
                    </fieldset>
                </form>
                <div id="back1"></div>
                <div id="back2"></div>
                <div id="back3"></div>
                <div id="back4"></div>
                <div id="back6"></div>

            </div>
        </div>
    </div>
</div>
<%@include file="../.././jsp.inc/footer.inc.jsp" %>
</body>
</html>