<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html" charset=UTF-8\>
    <meta charset="UTF-8">
    <title>Your account need to be activated</title>
    <link rel="stylesheet" href="../../css/panel.css">
    <link rel="stylesheet" href="../../css/footer.css">
    <style>
        #inputs input
        {
            background: url(https://cdn2.iconfinder.com/data/icons/ourea-icons/128/Email_256x256-32.png) no-repeat;
            background-size: 40px;

            padding: 15px 15px 15px 30px;
            margin: 0 0 10px 0;
            width: 88.25%;/*353px;  353 + 2 + 45 = 400 */
            border: 1px solid #ccc;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            border-radius: 5px;
            -moz-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
            -webkit-box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
            box-shadow: 0 1px 1px #ccc inset, 0 1px 0 #fff;
        }
        #inputs input:focus
        {
            background-color: #fff;
            border-color: #e8c291;
            outline: none;
            -moz-box-shadow: 0 0 0 1px #e8c291 inset;
            -webkit-box-shadow: 0 0 0 1px #e8c291 inset;
            box-shadow: 0 0 0 1px #e8c291 inset;
        }
        #submit
        {
            background-color: #ffb94b;
            background-image: -webkit-gradient(linear, left top, left bottom, from(#fddb6f), to(#ffb94b));
            background-image: -webkit-linear-gradient(top, #fddb6f, #ffb94b);
            background-image: -moz-linear-gradient(top, #fddb6f, #ffb94b);
            background-image: -ms-linear-gradient(top, #fddb6f, #ffb94b);
            background-image: -o-linear-gradient(top, #fddb6f, #ffb94b);
            background-image: linear-gradient(top, #fddb6f, #ffb94b);
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            text-shadow: 0 1px 0 rgba(255,255,255,0.5);
            -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
            -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
            box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
            border-width: 1px;
            border-style: solid;
            border-color: #d69e31 #e3a037 #d5982d #e3a037;
            float: left;
            height: 35px;
            padding: 0;
            width: 200px;
            cursor: pointer;
            font: bold 15px Arial, Helvetica;
            color: #8f5a0a;
            margin-top:10px;
        }

        #submit:hover,#submit:focus
        {
            background-color: #fddb6f;
            background-image: -webkit-gradient(linear, left top, left bottom, from(#ffb94b), to(#fddb6f));
            background-image: -webkit-linear-gradient(top, #ffb94b, #fddb6f);
            background-image: -moz-linear-gradient(top, #ffb94b, #fddb6f);
            background-image: -ms-linear-gradient(top, #ffb94b, #fddb6f);
            background-image: -o-linear-gradient(top, #ffb94b, #fddb6f);
            background-image: linear-gradient(top, #ffb94b, #fddb6f);
        }

        #submit:active
        {
            outline: none;
            -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
            -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
            box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
        }

        #submit::-moz-focus-inner
        {
            border: none;
        }

        #actions
        {
            margin: 25px 0 0 0;
        }
        fieldset
        {
            border: 0;
            padding: 0;
            margin: 0;
        }

        #genpass{
            background-color: #fff;
            background-image: -webkit-gradient(linear, left top, left bottom, from(#fff), to(#eee));
            background-image: -webkit-linear-gradient(top, #fff, #eee);
            background-image: -moz-linear-gradient(top, #fff, #eee);
            background-image: -ms-linear-gradient(top, #fff, #eee);
            background-image: -o-linear-gradient(top, #fff, #eee);
            background-image: linear-gradient(top, #fff, #eee);
            height: 100px;
            width: 300px;
            margin: -300px 0 0 -230px;
            padding: 30px;
            position: absolute;
            top: 50%;
            left: 50%;
            z-index: 0;
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            -webkit-box-shadow:
            0 0 2px rgba(0, 0, 0, 0.2),
            0 1px 1px rgba(0, 0, 0, .2),
            0 3px 0 #fff,
            0 4px 0 rgba(0, 0, 0, .2),
            0 6px 0 #fff,
            0 7px 0 rgba(0, 0, 0, .2);
            -moz-box-shadow:
            0 0 2px rgba(0, 0, 0, 0.2),
            1px 1px   0 rgba(0,   0,   0,   .1),
            3px 3px   0 rgba(255, 255, 255, 1),
            4px 4px   0 rgba(0,   0,   0,   .1),
            6px 6px   0 rgba(255, 255, 255, 1),
            7px 7px   0 rgba(0,   0,   0,   .1);
            box-shadow:
            0 0 2px rgba(0, 0, 0, 0.2),
            0 1px 1px rgba(0, 0, 0, .2),
            0 3px 0 #fff,
            0 4px 0 rgba(0, 0, 0, .2),
            0 6px 0 #fff,
            0 7px 0 rgba(0, 0, 0, .2);
        }
    </style>
    <%
        String form = " <form class=\"form\" method=\"post\" action=\"./forgetPassword.do\" id=\"genpass\">\n" +
                "                   <fieldset id=\"inputs\">\n" +
                "                   <input type=\"email\"  name=\"email\" value=\"\"/>\n" +
                "                   </fieldset>\n" +
                "                   <fieldset id=\"actions\">\n" +
                "                       <input type=\"submit\" name=\"btn\" id=\"submit\" value=\"Send new password\">\n" +
                "                   </fieldset>\n" +
                "\n" +
                "               </form>";
        String SendEmail = "Email with password has been sent ";

    %>
</head>
<body>
<div class="sb-site">
    <div class="main-content">
        <%
            /*
                            Cookie[] cookies = request.getCookies();
                            if(cookies!=null&&(request.getParameter("act")!=null))
                            {
                                for(Cookie c : cookies)
                                {
                                    if (c.getName().equals("timeBadEmail")) time = c.getValue();
                                }
                                if(LinksGenerator.Decode(request.getParameter("act")).equals("badEmail"+time))
                                {
                                    //out.print("<span style=\"margin-left:50px;\">Email with new password has been sent</span>");



                                }

                            }
                            else
                            {
                                out.print(form);
                            }*/
            try {
                HttpSession httpSession = request.getSession();

                if (request.getParameter("act")!=null) {
                    if((String) httpSession.getAttribute("timeBadEmail") != null)
                    { String time = (String) httpSession.getAttribute("timeBadEmail");
                        if (LinksGenerator.Decode(request.getParameter("act")).equals("badEmail" + time)) {
                            out.print("<span style=\"margin-left:50px;\">This Email address is not registered.<br>" +
                                    "To register <a href=\"\\registration.jsp\">Click</a> </span>");
                        }}
                    if (LinksGenerator.Decode(request.getParameter("act")).contains("SuccessfulSendEmail")) {
                        out.print("<span style=\"margin-left:20%;margin-top:50%;font-size:45px;color:green;\">" + SendEmail + "</span>");

                    } else {

                    }
                }
                else
                {
                    out.print(form);
                }
            }
            catch(NullPointerException e)
            {
                e.printStackTrace();
            }
        %>
        </div>

</div>
<%@include file="../../jsp.inc/footer.inc.jsp" %>
</body>
</html>