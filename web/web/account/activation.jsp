<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html" charset=UTF-8\>
    <title>Acctivation page</title>
    <link rel="stylesheet" href="../../css/activation.css">
    <link rel="stylesheet" href="../../css/footer.css">
    <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
</head>

<body>
<header>
    <h1>Welcome to Travelers Journal</h1>
</header>
<div class="sb-site">


    <div class="main-content">
        <%
            if (session.getAttribute("regLink") != null && request.getParameter("act") == null) {
                String reglink = (String) session.getAttribute("regLink");

                out.write("<input class=\"button\" type=\"submit\" value=\"Activate account\" " +
                        "onclick=\"window.location.href='" + reglink + "'\"/>");

            } else if (request.getParameter("act") != null) {
                String act = request.getParameter("act").trim();
                if (LinksGenerator.Decode(act).contains("ActivationFromLinkSuccessful")) {

                    out.write("<div id=\"SuccActiv\">");
                    out.write("<h1>Your account has been successfuly activated</h1>");
                    out.write("</div>");
                }
                if (LinksGenerator.Decode(act).contains("ExpiredActivationLink")) {

                    out.write("<div id=\"FalseActiv\">");
                    out.write("<h1>Expired activation link</h1>");
                    out.write("</div>");
                }
            } else
                response.sendError(404, "Not found");


        %>

    </div>
</div>

</body>
<%@include file="../../jsp.inc/footer.inc.jsp" %>
</html>