<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%
        String login = null;
        User user = null;
        String userID =null;
                String userName =null;

        if (session.getAttribute("login") == null||  session.getAttribute("UsrID")==null
                ||session.getAttribute("userName")==null) {
            response.sendRedirect("../auth/logout.do");
        }
        else
        {
            login = (String) session.getAttribute("login");
            userName = (String) session.getAttribute("userName");
             userID = (String) session.getAttribute("UsrID");

            if (userID == null)
                System.out.println("FUCK");
            else {
                try {
                    user = User.getUserByID(Integer.valueOf(userID));
                } catch (SQLException e) {

                }
            }

    %>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Edit your account information</title>
    <!---Styles--->
    <link rel="stylesheet" href="../../css/settings.css">
    <link rel="stylesheet" href="../../css/menu.css">
    <link rel="stylesheet" href="../../css/footer.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!---Styles--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="../../js/menu.js"></script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {$.slidebars();});}) (jQuery);

    </script>
</head>
<body>
<header>
    <span>
    <h3 class="header">Travelers Journal</h3>
    <h3 class="usrName">Hello, <%=userName%></h3>
        </span>
</header>
<div class="sb-slidebar sb-left">
    <a href="../panel.jsp">Main Panel</a>
    <a href="../stories/add.jsp">Add your travel story</a>
    <a href="../stories/my/index.jsp">Your stories</a>
    <a href="../feed/">New shared stories</a>
    <a href="">Account settings</a>
    <a href="../auth/logout.do">Sign out</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">
        <%
            if(user!=null)
            {
                out.print("<div class=\"usrEdit\">");
                out.print("<div class=\"usrEdit-heading\">" +
                        "Edit your account datas" +
                        "</div>");
                out.print("<form action=\"./edit.do\" method=\"post\">");
                out.print("<label for=\"firstName\">" +
                        "<span>First Name</span>" +
                        "<input type=\"text\" class=\"input-field\" name=\"firstName\" value=\""+LinksGenerator.Decode(user.getSurname())+"\" />" +
                        "<input type=\"text\" hidden=\"hidden\" name=\"userid\" value=\""+user.getUserID()+"\" />" +
                        "</label>");
                out.print("  <label for=\"lastName\">" +
                        "<span>Last Name</span>" +
                        "<input type=\"text\" class=\"input-field\" name=\"lastName\" value=\""+LinksGenerator.Decode(user.getName())+"\" />" +
                        "</label>");
                out.print("  <label for=\"email\"><span>Email</span><input type=\"email\" class=\"input-field\" name=\"email\" value=\""+LinksGenerator.Decode(user.getEmail())+"\" /></label>");
                out.print("  <label for=\"login\"><span>Login</span><input type=\"text\" readonly=\"readonly\" class=\"input-field\" name=\"login\" value=\""+LinksGenerator.Decode(user.getLogin())+"\" /></label>");
                out.print("  <label for=\"oldPass\"><span>Old password</span><input type=\"password\" class=\"input-field\" name=\"oldPass\" value=\"\" /></label>");
                out.print("  <label for=\"newPass\"><span>New password</span><input type=\"password\" class=\"input-field\" name=\"newPass\" value=\"\" /></label>");
                out.print("  <label for=\"confNewPass\"><span>Confirm</span><input type=\"password\" class=\"input-field\" name=\"confNewPass\" value=\"\" /></label>");
                out.print(" <label><span>&nbsp;</span><input type=\"submit\" value=\"Save changes\" /></label>");
                out.print(" </form>");
                out.print(" <div class=\"manipulate\">");
                out.print(" <label><span>&nbsp;</span><input type=\"button\" onclick=\"window.location ='./dacc.do?uid="+LinksGenerator.Encode(userID)+"&act="+LinksGenerator.Encode("deact")+"'\" value=\"Deactivate account\" id=\"Deactivate\"/></label>");
                out.print(" <label><span>&nbsp;</span><input type=\"button\" value=\"Delete account\" id=\"DeleteAcc\" onclick=\"window.location ='./dacc.do?uid="+LinksGenerator.Encode(userID)+"&act="+LinksGenerator.Encode("del")+"'\" /></label>");
                out.print(" </div>");
                out.print("  </div>");
            }
            else
                response.sendError(505,"USER OR USERID  NULL");
            }
        %>
    </div>
</div>

<%@include file="../.././jsp.inc/footer.inc.jsp" %>
</body>
</html>