<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.Story" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.User" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.time.Instant" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.City" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%
        String login = null;
        String address = null;

        User user = null;
        ArrayList<Story> stories=null;
        String userName =(String) session.getAttribute("userName");
        String userID =(String) session.getAttribute("UsrID");
        String sessionID = null;
        try {
            if (session.getAttribute("login") == null||  session.getAttribute("UsrID")==null)
            {
                response.sendRedirect("../../auth/logout.do");//http://www.travelersjournal.cz:8080/web/stories/web/auth/logout.do
            }
            else
            {
                // login =  session.getAttribute("login").toString();

                user = User.getUserByID(Integer.valueOf(userID));


                stories = User.getAllStoriesByUsrID(Integer.valueOf(userID));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    %>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Your stories</title>
    <!---Styles--->

    <link rel="stylesheet" href="../../../css/mystories.css">
    <link rel="stylesheet" href="../../../css/menu.css">
    <link rel="stylesheet" href="../../../css/footer.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!---Styles--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="../../../js/menu.js"></script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $.slidebars();
                $( "#SuccDialog" ).dialog({
                    autoOpen: false,
                    show: {
                        effect: "blind",
                        duration: 1000
                    },
                    hide: {
                        effect: "explode",
                        duration: 1000
                    }
                });
                $(".opener").click(function () {

                    $( ".SuccDialog" ).dialog( "open" );


                })
                $( "#accordion" ).accordion({
                    heightStyle: "content"
                });
            });

        }) (jQuery);




    </script>
</head>
<body>
<header>
    <span>
    <h3 class="header">Travelers Journal</h3>
    <h3 class="usrName">Hello, <%=userName%></h3>
        </span>
</header>
<div class="sb-slidebar sb-left">
        <a href="../../panel.jsp">Main Panel</a>
        <a href="../add.jsp">Add your travel story</a>
        <a href="./">My stories</a>
        <a href="../../feed/">New shared stories</a>
        <a href="../../account/">Account settings</a>
        <a href="../../auth/logout.do">Sign out</a>

</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">
        <%
int i = 0;
            if(stories!=null&&user!=null)
            {
                out.print("<div id=\"accordion\">");
                for(Story story: stories)
                {
                  if(story.getStoryPermissions().isForMeOnly()||story.getStoryPermissions().isAll())
                    {i++;
                        try {
                            out.print("<h3 id=\"story"+i+"\">Story #"+i+":</h3>");
                            if(story.getUsrID()==Integer.valueOf(userID))
                            out.print("<div>\n");
                            if(story.getStoryPermissions().isForOther())
                            {
                                out.print("<h3> Address</h3>\n");
                                out.print("<div>\n");
                                out.print(story.getAddress());
                                out.print("</div>\n");
                            }
                            out.print("<h3> Title:</h3>\n");
                            out.print("<div>\n");
                            out.print(" "+story.getTitleOfStory());
                            out.print(" </div>\n");
                            out.print("\n<h3>City:</h3>");
                            out.print("\n <div>");
                            out.print("\n "+ City.findCityByID(story.getCityID()));
                            out.print("\n </div>");
                            out.print("\n<h3> Dates:</h3>");
                            out.print("\n<div>");
                            out.print("\n From:"+story.getFromDate());
                            out.print("\n To:"+story.getToDate());
                            out.print("\n</div>");
                            out.print("\n<h3> Rating:</h3>");
                            out.print("\n<div>");
                            out.print("\n "+story.StarsToString(story.getRating() ) );
                            out.print("\n</div>");
                            out.print("\n<h3>Text:</h3>");
                            out.print("\n<div>");
                            out.print("\n "+story.getStory().toString());
                            out.print("\n</div> ");
                            out.print("\n<h3 style=\"margin-left:85%;\">Creation Data</h3>");
                            out.print("\n<div style=\"margin-left:85%;\">");
                            out.print("\n Created: "+ Instant.ofEpochSecond(story.getStamp()));
                            out.print("\n <br>By:"+ LinksGenerator.Decode(user.getLogin()));
                            out.print("\n<div id=\"manipulate\">");
                            out.print("<br><a id=\"edit\" href=\"../edit.jsp?sid="+story.getStoryID()+"\">Edit</a>");
                            out.print("<a id=\"delete\" href=\"../delstory.do?id="+story.getStoryID()+"\">Delete</a>");
                            out.print("\n</div> ");
                            out.print("\n</div>");
                            out.print("</div>\n");


                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            else
                out.print("<h1 style=\"color:red;\">ERROR</h1>");


        %>
    </div>
    </div>
</div>

<%@include file="../../../jsp.inc/footer.inc.jsp" %>
</body>

</html>


