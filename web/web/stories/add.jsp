<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%
        String login = null;
        String address =(String) session.getAttribute("address");
        String other=(String) session.getAttribute("other");
        if (session.getAttribute("login") == null||  session.getAttribute("UsrID")==null) {
            response.sendRedirect("../../web/auth/logout.do");
        } else login = (String) session.getAttribute("login");

        String userName =(String) session.getAttribute("userName");
        String userID =(String) session.getAttribute("UsrID");
    %>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Add your story</title>
    <!---Styles--->

    <link rel="stylesheet" href="../.././css/add.css">
    <link rel="stylesheet" href="../.././css/menu.css">
    <link rel="stylesheet" href="../.././css/footer.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!---Styles--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="../.././js/menu.js"></script>
    <%@include file="../.././jsp.inc/add.inc.jsp" %>
    <script type="text/javascript">
(function($) {
    $(document).ready(function() {


    })})(jQuery)
    </script>
</head>
<body>
<header>
    <span>
    <h3 class="header">Travelers Journal</h3>
    <h3 class="usrName">Hello, <%=userName%></h3>
        </span>
</header>
<div class="sb-slidebar sb-left">
        <a href="../panel.jsp">Main Panel</a>
        <a href="add.jsp">Add your travel story</a>
        <a href="./my/">Your stories</a>
        <a href="../feed">New shared stories</a>
        <a href="../account">Account settings</a>
        <a href="../auth/logout.do">Sign out</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">
        <%



            out.print(
                    " <form action=\"add.do\" method=\"post\" name=\"addStory\" class=\"addStory lifted\" id=\"addStory\">\n"
                    +
                    "                        <div id=\"headOfStory\">\n"
                    +
                    "                        <label for=\"inputStory\"><h3>Enter name of your story:</h3></label>\n"
                    +
                    "                            <input type=\"text\" required=\"required\" value=\"\" name=\"headOfStory\" width=\"500\"id=\"inputStory\"/>\n"
                    +
                    "                        </div>\n"
                    +
                    "                        <div id=\"controls\">\n"
                    +
                    "                        <div id=\"perm\">\n"
                    +
                    "<div id=\"radio\">\n" +
                    "    <input type=\"radio\" value=\"All\" id=\"ForAll\" name=\"radio\" class=\"ForAll\"><label for=\"ForAll\" class=\"ForAll\">Share story for all registred</label>\n" +
                    "    <input type=\"radio\" value=\"MeOnly\" id=\"ForMe\" name=\"radio\" checked=\"checked\"class=\"ForMe\"><label for=\"ForMe\" class=\"ForMe\">Save only for me</label>\n" +
                    "  </div>"+
                    "<div id=\"checkbox\">\n" +
                    "                            <input type=\"checkbox\" id=\"Other\" class=\"Other\" name=\"other\"><label for=\"Other\" class=\"Other\">Other</label>\n"
                    +
                    "</div>\n" +
                    "                        </div>\n"
                    +
                    "                            <select required=\"required\" id=\"rating\" name=\"rating\">\n"
                    +
                    "                                <option value=\"Bad\">Bad</option>\n"
                    +
                    "                                <option value=\"Good\">Good</option>\n"
                    +
                    "                                <option value=\"VeryGood\">Very good</option>\n"
                    +
                    "                                <option value=\"Excellent\">Excellent</option>\n"
                    +
                    "                            </select>\n"
                    +
                    "                        <div id=\"calendar\">\n"
                    +
                    "                            <label for=\"from\">From</label>\n"
                    +
                    "                            <input type=\"text\" id=\"from\" name=\"from\">\n"
                    +
                    "                            <label for=\"to\">to</label>\n"
                    +
                    "                            <input type=\"text\" id=\"to\" name=\"to\">\n"
                    +
                    "                        </div>\n"
                    +
                    "                        <div id=\"ui-widget\" >\n"
                    +
                    "                            <label for=\"cityList\">City:</label>\n"
                    +
                    "                            <input id=\"cityList\" name=\"city\" required=\"required\">\n"
                    +
                    "                        </div>\n"
                    +
                    "                        </div>\n"
                    +
                    "                            <textarea name=\"story\">\n"
                    +
                    "                            </textarea>\n"
                    +
                    "                        <input type=\"submit\" value=\"Save to my stories\">\n");
            if (request.getParameter("act") != null) {

                out.print("  <input type=\"submit\" value=\"Get address\"/ id=\"opener\">");
                if (LinksGenerator.Decode(request.getParameter("act")).contains("&Other=0")) {
                    out.print("<div id=\"FalseDialog\" title=\"Forbidden\">");
                    out.print("<h3>Change setting of this story to share her address</h3>");
                    out.print(" </div>");

                }
                else if (LinksGenerator.Decode(request.getParameter("act")).contains("AddStorySuccessful")) {
                    out.print("<div id=\"SuccDialog\" title=\"Address of story\">");
                    out.print("  <input type=\"text\" value=\""+address+"\"/>");
                    out.print(" </div>");

                }
            }

            out.print("</form>");


        %>
    </div>
</div>
<%@include file="../.././jsp.inc/footer.inc.jsp" %>
</body>

</html>