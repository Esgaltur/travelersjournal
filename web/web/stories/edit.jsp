<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.Story" %>
<%@ page import="java.sql.SQLException" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%
        String login = null;
        String address =(String) session.getAttribute("address");
        String other=(String) session.getAttribute("other");
        String userName = null;
        String sessionID = null;
        Story story = null;
        if (session.getAttribute("login") == null|| session.getAttribute("UsrID")==null) {
            response.sendRedirect("../../web/auth/logout.do");
        } else login = (String) session.getAttribute("login");

        userName =(String) session.getAttribute("userName");
       int StoryID = Integer.parseInt(request.getParameter("sid"));
        try {
         story = Story.getStoryByID(StoryID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    %>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Edit your story</title>
    <!---Styles--->

    <link rel="stylesheet" href="../../css/add.css">
    <link rel="stylesheet" href="../../css/menu.css">
    <link rel="stylesheet" href="../../css/footer.css">


    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!---Styles--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>


    <script src="../.././js/menu.js"></script>
    <%@include file="../.././jsp.inc/add.inc.jsp" %>
    <script type="text/javascript">


        (function($) {
            $(document).ready(function() {


            })})(jQuery)
    </script>
</head>
<body>
<header>
    <span>
    <h3 class="header">Travelers Journal</h3>
    <h3 class="usrName">Hello, <%=userName%></h3>
        </span>
</header>
<div class="sb-slidebar sb-left">
    <a href="../panel.jsp">Main Panel</a>
    <a href="add.jsp">Add your travel story</a>
    <a href="./my/">Your stories</a>
    <a href="../feed">New shared stories</a>
    <a href="../account">Account settings</a>
    <a href="../auth/logout.do">Sign out</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">
        <%
if(story!=null)
{



    out.print(" <form action=\"./editstory.do\" method=\"post\" name=\"editStory\" class=\"editStory \" id=\"editStory\">\n");
    out.print(  "<div id=\"headOfStory\">\n");
    out.print(  "<input type=\"text\" hidden=\"hidden\" name=\'storyID\' value=\""+story.getStoryID()+"\">\n");
    out.print( "<label for=\"inputStory\"><h3>Name of your story:</h3></label>\n");
    out.print( "<input type=\"text\" required=\"required\" value=\""+story.getTitleOfStory()+"\" name=\"headOfStory\" width=\"500\"id=\"inputStory\"/>\n");
    out.print(  "</div>\n");
    out.print( " <div id=\"controls\">\n");
    out.print("<div id=\"perm\">\n");
    out.print( "<div id=\"radio\">\n" );
    String All = story.getStoryPermissions().isAll()?"checked":" ";
    String me = story.getStoryPermissions().isForMeOnly()?"checked":" ";
    out.print("<input type=\"radio\" value=\"All\" id=\"ForAll\" name=\"radio\" " + All + " class=\"ForAll\"><label for=\"ForAll\" class=\"ForAll\">Share story for all registred</label>\n" );
    out.print(  "<input type=\"radio\" value=\"MeOnly\" id=\"ForMe\" name=\"radio\" "+me+" class=\"ForMe\"><label for=\"ForMe\" class=\"ForMe\">Save only for me</label>\n" );
    out.print(  "</div>");
    out.print( "<div id=\"checkbox\">\n" );
    String otherChecked = story.getStoryPermissions().isForOther()?"checked":" ";
    out.print(  "<input type=\"checkbox\" id=\"Other\" class=\"Other\""+otherChecked+" name=\"other\"><label for=\"Other\" class=\"Other\">Other</label>\n");
    out.print( "</div>\n" );
    out.print(  "</div>\n");
    out.print(   "<select required=\"required\" id=\"rating\" name=\"rating\">\n");
   switch(story.getRating())
   {
       case 2:
       {
           out.print(   "<option value=\"Bad\" selected=\"Bad\">Bad</option>\n");
           out.print(  "<option value=\"Good\">Good</option>\n");
           out.print(  "<option value=\"VeryGood\">Very good</option>\n");
           out.print(   "<option value=\"Excellent\">Excellent</option>\n");
           break;
       }
       case 3:
       {
           out.print(   "<option value=\"Bad\">Bad</option>\n");
           out.print(  "<option value=\"Good\" selected=\"Good\">Good</option>\n");
           out.print(  "<option value=\"VeryGood\">Very good</option>\n");
           out.print(   "<option value=\"Excellent\">Excellent</option>\n");
           break;
       }
       case 4:
       {
           out.print(   "<option value=\"Bad\" >Bad</option>\n");
           out.print(  "<option value=\"Good\">Good</option>\n");
           out.print(  "<option value=\"VeryGood\" selected=\"VeryGood\">Very good</option>\n");
           out.print(   "<option value=\"Excellent\">Excellent</option>\n");
           break;
       }
       case 5:
       {
           out.print(   "<option value=\"Bad\" >Bad</option>\n");
           out.print(  "<option value=\"Good\">Good</option>\n");
           out.print(  "<option value=\"VeryGood\">Very good</option>\n");
           out.print(   "<option value=\"Excellent\" selected=\"Excellent\">Excellent</option>\n");
           break;
       }
       default:
       {
           out.print(   "<option value=\"Bad\" >Bad</option>\n");
           out.print(  "<option value=\"Good\">Good</option>\n");
           out.print(  "<option value=\"VeryGood\">Very good</option>\n");
           out.print(   "<option value=\"Excellent\">Excellent</option>\n");
           break;
       }

   }
    out.print( "</select>\n");
    out.print("<div id=\"calendar\">\n");
    out.print( "<label for=\"from\">From</label>\n");
    out.print( "<input type=\"text\" id=\"from\" name=\"from\" value=\""+story.getFromDate()+"\">\n");
    out.print( "<label for=\"to\">to</label>\n");
    out.print( "<input type=\"text\" id=\"to\" name=\"to\" value=\""+story.getToDate()+"\">\n");
    out.print(  "</div>\n");
    out.print( "<div id=\"ui-widget\" >\n");
    out.print( " <label for=\"cityList\">City:</label>\n");
    try {
        out.print( "<input id=\"cityList\" name=\"city\" required=\"required\" value=\""+ City.odstrDiak(City.getCityNameByID(story.getCityID()))+"\"/>\n");
    } catch (SQLException e) {
        e.printStackTrace();
    }
    out.print(  "</div>\n");
    out.print( "</div>\n");
    out.print( "<textarea name=\"story\">\n");
    String[] lines = story.getStory().toString().split("\\n");
    for(String s: lines){out.print("\n " + s);}
    out.print( "</textarea>\n");
    out.print(  "<input type=\"submit\" value=\"Edit\">\n");
    out.print("</form>");
}
            else
out.print("<h1>ERROR</h1>");
        %>
    </div>
</div>
<%@include file="../.././jsp.inc/footer.inc.jsp" %>
</body>

</html>