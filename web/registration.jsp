<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<%@ page import="cz.esgaltur.webApp.servlets.util.RegistrationFormField" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title></title>
    <link rel="stylesheet" href="css/RegStyle.css">
    <link rel="stylesheet" href="css/footer.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>

    <%@include file="jsp.inc/form.inc.jsp" %>
</head>
    <header>
        <h3 class="header">Travelers Journal</h3>
    </header>
<body>
    <div class="main-content ">
        <%@include file="jsp.inc/regForm.inc.jsp" %>
                <div class="backgroundPict"></div>
            </div>
<%@include file="jsp.inc/footer.inc.jsp" %>
</body>
</html>