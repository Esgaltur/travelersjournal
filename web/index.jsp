
<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Travelers Journal</title>
    <!--Styles--->
    <link rel="stylesheet" href="css/indexStyle.css">
    <link rel="stylesheet" href="css/menu.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/footer.css">
    <!--Styles--->
    <!--JavaScript--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/menu.js"></script>
     <script type="text/javascript" src="http://jqueryrotate.googlecode.com/svn/trunk/jQueryRotate.js"></script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $.slidebars();


            });

        }) (jQuery);
    </script>
    <!--JavaScript--->
</head>

<body>
<header>
      <span>
    <h3 class="header">Travelers Journal</h3>
        </span>
</header>
<div class="sb-slidebar sb-left">
    <a href="./">Home</a>
    <a href="./web/auth/">Auth</a>
    <a href="registration.jsp">Registration</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">
       <h1 style="color:#DAA520; margin-left:600px">Welcome to Travelers Journal</h1>
    </div>
</div>
<%@ include file="./jsp.inc/footer.inc.jsp" %>
</body>

</html>