<script type="text/javascript">
var cities =[<%@include file="./citiesSource.inc.jsp" %>];

var other = <%=other%>;
(function($) {
    $(document).ready(function() {
        $.slidebars();

        var NoResultsLabel = "No Results";
        $("#cityList").autocomplete({
            source: function(request, response) {
                var results = $.ui.autocomplete.filter(cities, request.term);
                if (!results.length) {
                    results = [NoResultsLabel];
                }

                response(results);
            },
            select: function (event, ui) {
                if (ui.item.label === NoResultsLabel) {
                    event.preventDefault();
                    $("#cityList").val(" ");
                }
            },
            focus: function (event, ui) {
                if (ui.item.label === NoResultsLabel) {
                    event.preventDefault();
                    $("#cityList").val(" ");
                }
            }
        });
        $("#radio").buttonset();
        $("#checkbox").buttonset();
        $("#from").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onClose: function (selectedDate) {
                $("#to").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#to").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onClose: function (selectedDate) {
                $("#from").datepicker("option", "maxDate", selectedDate);
            }
        });
        $( "#SuccDialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });
        $( "#FalseDialog" ).dialog({
            autoOpen: false,
            show: {
                effect: "blind",
                duration: 1000
            },
            hide: {
                effect: "explode",
                duration: 1000
            }
        });
        $("#opener").click(function () {
            if(other==true)
            {
                $( "#SuccDialog" ).dialog( "open" );
            }
        else
            {
                $( "#FalseDialog" ).dialog( "open" );
            }

        })
    });


}) (jQuery);



</script>