<%
    if (request.getParameter("act") != null) {

        if (LinksGenerator.Decode(request.getParameter("act")).contains("RegSuccessful")) {
            out.print("<div id=\"RegSucc\">" +
                    "<h3>Registraton successful</h3>" +

                    "<p>Please check your email for activation mail</div>");
        } else {
            out.print("<form action=\"registration.do\" method=\"post\" class=\"registration\">\n" +
                    "    <h3 class=\"Text\">Registration</h3>");
            if (LinksGenerator.Decode(request.getParameter("act")).contains("BadSurname")) {
                out.print(RegistrationFormField.getSurname(false));
            } else {
                out.print(RegistrationFormField.getSurname(true));
            }
            if (LinksGenerator.Decode(request.getParameter("act")).contains("BadName")) {
                out.print(RegistrationFormField.getName(false));
            } else {
                out.print(RegistrationFormField.getName(true));
            }
            if (LinksGenerator.Decode(request.getParameter("act")).contains("ExistingEmail")) {
                out.print(RegistrationFormField.getEmail(false));
            } else {
                out.print(RegistrationFormField.getEmail(true));
            }
            if (LinksGenerator.Decode(request.getParameter("act")).contains("BadOrExistingLogin")) {
                out.print(RegistrationFormField.getLogin(false));
            } else {
                out.print(RegistrationFormField.getLogin(true));
            }
            if (LinksGenerator.Decode(request.getParameter("act")).contains("BadPassword")) {
                out.print(RegistrationFormField.getPassword(false));
            } else {
                out.print(RegistrationFormField.getPassword(true));
            }
            out.print("    <div class=\"field\">\n" +
                    "        <label for=\"confpas\">Confirm Password::</label>\n" +
                    "        <input type=\"password\" value=\"\" name =\"confPass\"   id=\"confpas\" class=\"input\">\n" +
                    "    </div>" +
                    "   <input type=\"submit\" class=\"button\" value=\"Register\">" +
                    "</form>");
        }


    } else {
        out.print(form);
    }
%>

