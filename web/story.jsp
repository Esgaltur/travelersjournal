<%@ page import="cz.esgaltur.webApp.servlets.classes.Story" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.City" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.User" %>
<%@ page import="java.time.Instant" %>
<%@ page import="cz.esgaltur.webApp.servlets.util.LinksGenerator" %>
<%@ page import="cz.esgaltur.webApp.servlets.classes.Comments" %>
<%@ page import="java.util.ArrayList" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <%/*story.jsp?id=3746*/
        int StoryID = 0;
        User user=null ;
        Story story = null;
        User usrCom = null;
        ArrayList<Comments> comments=null;
        if(request.getParameter("id")==null)
        {
            response.sendError(404," Story not Found");
        }
        if(request.getParameter("id")!=null)
        {
            StoryID = Integer.valueOf(request.getParameter("id").trim());
            user=null ;
            story = null;
            try {
                story = Story.getStoryByID(StoryID);
                user = User.getUserByStoryID(StoryID);

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }



    %>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Story</title>
    <!---Styles--->

    <link rel="stylesheet" href="./css/story.css">
    <link rel="stylesheet" href="./css/menu.css">
    <link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
    <link href='http://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'>
    <!---Styles--->
    <script src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="./js/menu.js"></script>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {

                $.slidebars();
                $( "#accordion" ).accordion({
                    heightStyle: "content"
                });

            });

        }) (jQuery);
    </script>
</head>
<body>
<header>
    <span>
    <h3 class="header">Travelers Journal</h3>
        </span>
</header>
<div class="sb-slidebar sb-left">
     <a href="./">Home</a>
     <a href="./web/auth/">Auth</a>
     <a href="registration.jsp">Registration</a>
</div>

<div id="sb-site">
    <div class="sb-toggle-left">
        <span class="bar"></span>
        <span class="bar"></span>
        <span class="bar"></span>
    </div>
    <div class="main-content">

        <%

            if(story!=null&&user!=null)
            {
                if(story.getStoryPermissions().isForOther())
                {
                    out.print("<div id=\"accordion\">");
                    try {
                        comments = Comments.getAllCommentsByStoryID(story.getStoryID());
                        out.print("<h3>Story:</h3>\n");
                        out.print("<div>\n");
                        out.print("<h3> Title:</h3>\n");
                        out.print("<div>\n");
                        out.print(" "+story.getTitleOfStory());
                        out.print(" </div>\n");
                        out.print("<h3> Permissions::</h3>");
                        out.print("<div>");
                        out.print(" For All:"+story.getStoryPermissions().isAll()+"");
                        out.print(" <br>Me only:"+story.getStoryPermissions().isForMeOnly());
                        out.print("<br> Other:"+story.getStoryPermissions().isForOther());
                        out.print("\n</div>");
                        out.print("\n<h3>City:</h3>");
                        out.print("\n <div>");
                        out.print("\n "+City.findCityByID(story.getCityID()));
                        out.print("\n </div>");
                        out.print("\n<h3> Dates:</h3>");
                        out.print("\n<div>");
                        out.print("\n From:"+story.getFromDate());
                        out.print("\n To:"+story.getToDate());
                        out.print("\n</div>");
                        out.print("\n<h3> Rating:</h3>");
                        out.print("\n<div>");
                        out.print("\n "+story.StarsToString(story.getRating() ) );
                        out.print("\n</div>");
                        out.print("\n<h3>Text:</h3>");
                        out.print("\n<div style=\"height:auto;\">");
                        String[] lines = story.getStory().toString().split("\\n");
                        for(String s: lines){out.print("\n " + s);}
                        out.print("\n</div> ");
                        out.print("\n<h3 style=\"margin-left:85%;\">Creation Data</h3>");
                        out.print("\n<div style=\"margin-left:85%;\">");
                        out.print("\n Created: "+ Instant .ofEpochSecond(story.getStamp()));
                        out.print("\n <br>By:"+ LinksGenerator.Decode(user.getLogin()));
                        out.print("\n</div>");
                        out.print("\n<h3>Comments</h3>");
                        out.print("\n<div>");

                        if(comments==null||comments.size()==0)
                        {
                            out.print("\nNo comments");
                        }
                        else
                        {
                            for(Comments comment:comments)
                            {  try {
                                usrCom = User.getUserByID(comment.getUsrID());
                                if(comment.isForAll())
                                {
                                    out.print("<h3>"+LinksGenerator.Decode(usrCom.getLogin())+"</h3>");
                                    out.print("\n<div>");
                                    out.print(comment.getComment());
                                    out.print("</div>");
                                }

                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            }
                        }
                        out.print(" \n </div>");

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    out.print("\n</div>");
                    out.print("\n</div>");
                }
                else response.sendError(404,"This story, id = "+StoryID+" DoesntExists");
            }
            else
                response.sendError(404,"This story, id = "+StoryID+" Doesnt Exists");
        %>
        </div>
</div>
<%@include file="./jsp.inc/footer.inc.jsp" %>
</body>

</html>